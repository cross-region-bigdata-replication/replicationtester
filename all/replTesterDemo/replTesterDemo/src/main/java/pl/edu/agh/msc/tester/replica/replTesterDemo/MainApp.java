package pl.edu.agh.msc.tester.replica.replTesterDemo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.MainDbFiller;
import pl.edu.agh.msc.tester.replica.replTesterDemo.scenario.MainTestScenario;

import java.util.Scanner;

@SpringBootApplication
public class MainApp {
    public static void main(String[] args) {
        System.out.println("Choose the running mode (0 - database generator, refiller; 1 - database tester");
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();

        switch (i) {
            case 0:
                MainDbFiller.main(args);
                break;
            case 1:
                MainTestScenario.main(args);
                break;
            default:
                System.out.println("You passed the wrong case. App will exit");
                break;
        }
        System.exit(0);
    }
}
