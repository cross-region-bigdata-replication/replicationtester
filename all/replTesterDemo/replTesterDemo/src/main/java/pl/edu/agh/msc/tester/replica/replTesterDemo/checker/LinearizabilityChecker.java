package pl.edu.agh.msc.tester.replica.replTesterDemo.checker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Request;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Vertex;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.DiGraph;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Maciej Makowka on 08.06.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.checker
 * Time 13:13
 */
public class LinearizabilityChecker {

    private static final Logger logger = LoggerFactory.getLogger(LinearizabilityChecker.class);

    private DiGraph graph = new DiGraph();

    public LinearizabilityChecker(DiGraph graph) {
        this.graph = graph;
    }

    private int errors = 0;

    // all requests to one object
    // sorted by their invocation time
    public int linearizableCheck(List<Request> unsortedRequest) {

        List<Request> sortedRequests = sortRequests(unsortedRequest);

        for (Request request : sortedRequests) {
            addToGraph(request);
            if (request.isREAD()) {
                // look ahead for concurrent writes
                for (Request wRequest : sortedRequests) {
                    if (wRequest.isWRITE() && areLinearConcurrent(request, wRequest))
                        graph.addVertex(new Vertex(wRequest));
                }

                // find matched write
                Vertex matchedWrite = findMatchedWrite(request);
                if (matchedWrite != null) // TODO: I did this fix, check whether it don't break down results
                    mergeReadIntoWrite(request, matchedWrite);

                if (foundAnomaly()) {
                    addError();
                }
            }
        }
//        System.out.println(graph);
        return errors;
    }

    private void addError() {
        errors++;
    }

    private boolean areLinearConcurrent(Request request, Request wRequest) {
        return !(request.getRespT().before(wRequest.getInvocT()))
                && !(wRequest.getRespT().before(wRequest.getInvocT()));
    }

    private List<Request> sortRequests(List<Request> unsortedRequest) {
        unsortedRequest.sort((o1, o2) -> {
            if (o1.getInvocT().equals(o2.getInvocT()))
                return 0;
            if (o1.getInvocT().before(o2.getInvocT()))
                return -1;
            return 1;
        });
        logger.debug("SO_REQ:" + unsortedRequest);
        return unsortedRequest;
    }


    private void addToGraph(Request request) {
        Vertex requestVertex = graph.getVertex(request);
        if (graph.contains(requestVertex))
            return; // already in graph from lookahead
        Vertex tempVertex = new Vertex(request);
        Vertex newVertex = graph.addVertex(tempVertex);
        // add edges from real-time ordering
        for (Vertex v : graph.verticles()) {
            if (v.getRespT().before(newVertex.getInvocT()))
                graph.addEdge(v, newVertex, 1);
        }
    }

    // matched write inherits edges read
    private void mergeReadIntoWrite(Request readRequest, Vertex matchedWrite) {
        Vertex readVertex = graph.getVertex(readRequest);
        for (DiGraph.Edge<Vertex> e : graph.getInEdges(readVertex))
            if (e.getSrcVertex() != matchedWrite)
                graph.addEdge(e.getSrcVertex(), matchedWrite, 1);
        /* Refine response time of write matching the read.
         * This allows to add further returns-before edges
         * and then spot possible cycles due to anomalies
         * that otherwise would go unnoticed (e.g. new-old inversion). */
        if (readRequest.getRespT().before(matchedWrite.getRespT()))
            matchedWrite.setRespT(readRequest.getRespT());

        // remove read operation from graph
        graph.removeVertex(readVertex);
    }

//    private Vertex findMatchedWrite(Request request) {
//        Vertex requestVertex = graph.getVertex(request);
//        for (Vertex v : graph.breadthFirstSearch(requestVertex)) {
//            System.out.println("coś tam przeszukuje");
//            if (match(v.getRequest(), request)) {
//                System.out.println("bedzie V");
//                return v;
//            }
//        }
//        System.out.println("bedzie null");
//        return null;
//    }

    private Vertex findMatchedWrite(Request request) {
        Vertex requestVertex = graph.getVertex(request);
//        for (Vertex v : graph.breadthFirstSearch(requestVertex)) {
        for (Vertex v : graph.verticles()) {
            if (match(v.getRequest(), requestVertex.getRequest())) {
//                System.out.println("bedzie V");
                return v;
            }
        }
//        System.out.println("bedzie null");
        return null;
    }

    private boolean match(Request request1, Request request2) {
        return ((request1.isWRITE() && request2.isREAD()) || (request1.isREAD() && request2.isWRITE()))
                && request1.getArg() == request2.getArg();
    }

    // cycles indicate no legal total order
    // exists and this read is an anomaly
    private boolean foundAnomaly() {
//        cycles = graph.checkForCycles();
//        if (cycles == null)
//            return false;
//        // remove edges that produced cycles
//        for (c:
//             cycles) {
//            for (e:
//                 cycles.edges())
//                if (e.source().getInvocT().before(e.destination().getRespT()))
//                    graph.removeEdge(e);
//        }
        // TODO: Implement it like in is in Lu 2015.
        return graph.checkForCycles();
    }
}
