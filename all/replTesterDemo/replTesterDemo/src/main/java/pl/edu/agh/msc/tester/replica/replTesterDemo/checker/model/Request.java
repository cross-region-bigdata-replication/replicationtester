package pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Maciej Makowka on 08.06.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model
 * Time 13:14
 */
public class Request {

    private final UUID id;
    private final boolean READ;
    private final boolean WRITE;
    private final String query;
    private Timestamp invocT;
    private Timestamp respT;
    private int arg;

    public Request(UUID id, boolean WRITE, String query) {
        this.id = id;
        this.WRITE = WRITE;
        this.READ = !WRITE;
        this.query = query;
    }

    public UUID getId() {
        return id;
    }

    public boolean isREAD() {
        return READ;
    }

    public boolean isWRITE() {
        return WRITE;
    }

    public String getQuery() {
        return query;
    }

    public Timestamp getInvocT() {
        return invocT;
    }

    public Timestamp getRespT() {
        return respT;
    }

    public void setInvocT(Timestamp invocT) {
        this.invocT = invocT;
    }

    public void setRespT(Timestamp respT) {
        this.respT = respT;
    }

    public void setArg(int arg) {
        this.arg = arg;
    }

    public int getArg() {
        return arg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return isREAD() == request.isREAD() &&
                isWRITE() == request.isWRITE() &&
                getArg() == request.getArg() &&
                Objects.equals(getId(), request.getId()) &&
                Objects.equals(getQuery(), request.getQuery()) &&
                Objects.equals(getInvocT(), request.getInvocT()) &&
                Objects.equals(getRespT(), request.getRespT());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), isREAD(), isWRITE(), getQuery(), getInvocT(), getRespT(), getArg());
    }

    @Override
    public String toString() {
        return "Request{" +
                "" + WRITE +
                ", invocT=" + invocT +
                ", respT=" + respT +
                ", arg=" + arg +
                '}';
    }

    //    @Override
//    public String toString() {
//        return "Request{" +
//                "query='" + query + '\'' +
//                ", invocT=" + invocT +
//                ", respT=" + respT +
//                ", arg=" + arg +
//                '}';
//    }
}
