package pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model;

import java.sql.Timestamp;
import java.util.Objects;

public class Vertex {

    private Request request;

    public Vertex(Request request) {
        this.request = request;
    }

    public Request getRequest() {
        return request;
    }

    public Timestamp getInvocT() {
        return request.getInvocT();
    }

    public Timestamp getRespT() {
        return request.getRespT();
    }

    public int getArg() {
        return request.getArg();
    }

    public void setInvocT(Timestamp invocT) {
        request.setInvocT(invocT);
    }

    public void setRespT(Timestamp respT) {
        request.setRespT(respT);
    }

    public void setArg(int arg) {
        request.setArg(arg);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(getRequest(), vertex.getRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRequest());
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "request=" + request +
                '}';
    }
}
