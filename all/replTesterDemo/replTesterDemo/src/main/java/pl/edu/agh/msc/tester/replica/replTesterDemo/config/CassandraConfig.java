package pl.edu.agh.msc.tester.replica.replTesterDemo.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.AddressTranslator;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.RoundRobinPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.config.extension.SkewedTimestampGenerator;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;

import java.net.InetSocketAddress;

public class CassandraConfig {

    private static final Logger logger = LoggerFactory.getLogger(CassandraConfig.class);

    private final String DATA_CENTER_HOST = LoadProperty.getString("cassandra.data_center_host");
    private final String KEYSPACE_NAME = LoadProperty.getString("cassandra.keyspace_name");
    private final String DATA_TABLE_NAME = LoadProperty.getString("cassandra.table_name1");
    private final String REPLICATION_FACTOR = LoadProperty.getString("cassandra.replication_factor");
    private final String REPLICATION_CLASS = LoadProperty.getString("cassandra.replication_class");
    private final String CONSISTENCY_LVL = LoadProperty.getString("cassandra.lvl");

    private final boolean ROUND_ROBIN = LoadProperty.getBoolean("cassandra.round_robin");

    private final String KEYSPACE_CREATION_QUERY = "CREATE KEYSPACE IF NOT EXISTS " + KEYSPACE_NAME + " " +
            "WITH replication = { 'class': '" + REPLICATION_CLASS + "', 'replication_factor': '" + REPLICATION_FACTOR + "' };";
    private final String KEYSPACE_DELETION_QUERY = "DROP KEYSPACE IF EXISTS " + KEYSPACE_NAME + ";";

    private final int SEED_PORT = LoadProperty.getInt("cassandra.port1");
    private final int DB1_PORT = LoadProperty.getInt("cassandra.port2");
    private final int DB2_PORT = LoadProperty.getInt("cassandra.port3");
    private final String SEED_INTERNAL_IP = LoadProperty.getString("cassandra.internal_ip_host1");
    private final String DB1_INTERNAL_IP = LoadProperty.getString("cassandra.internal_ip_host2");
    private final String DB2_INTERNAL_IP = LoadProperty.getString("cassandra.internal_ip_host3");

    public CassandraConfig() {
    }

    private String getKeyspaceName() {
        return KEYSPACE_NAME;
    }

    private AddressTranslator getCloudAddressTranslator() {
        AddressTranslator addressTranslator = new AddressTranslator() {
            @Override
            public void init(Cluster cluster) {
                // TODO:
            }

            @Override
            public InetSocketAddress translate(InetSocketAddress inetSocketAddress) {
                String ADDRESS_OUT = DATA_CENTER_HOST;

                if (inetSocketAddress.getHostName().compareTo(SEED_INTERNAL_IP) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, SEED_PORT);
                if (inetSocketAddress.getHostName().compareTo(DB1_INTERNAL_IP) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, DB1_PORT);
                if (inetSocketAddress.getHostName().compareTo(DB2_INTERNAL_IP) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, DB2_PORT);
                // TODO: Add logging the bad address. Catch this problem.
                logger.error("BAD ADDRESS, NO MATCH :: " + inetSocketAddress.getHostName());
                return null;
            }

            @Override
            public void close() {
                // TODO:
            }
        };
        return addressTranslator;

    }

    public Cluster getCluster() {

        QueryOptions qo = getConsistencyLevel();

        return Cluster.builder()
                .addContactPoint(DATA_CENTER_HOST)
                .withPort(SEED_PORT)
                .withAddressTranslator(getCloudAddressTranslator())
                .withLoadBalancingPolicy(getLoadBalancingPolicy())
                .withQueryOptions(qo)
                .build();
    }

    public Cluster getSkewedCluster() {
        QueryOptions qo = new QueryOptions().setConsistencyLevel(ConsistencyLevel.ONE);
        return Cluster.builder()
                .addContactPoint(DATA_CENTER_HOST)
                .withPort(SEED_PORT)
                .withAddressTranslator(getCloudAddressTranslator())
                .withLoadBalancingPolicy(getLoadBalancingPolicy())
                .withQueryOptions(qo)
                .withTimestampGenerator(new SkewedTimestampGenerator())
                .build();
    }

    private LoadBalancingPolicy getLoadBalancingPolicy() {
        return new RoundRobinPolicy();
    }

    public void deleteDefaultKeyspace(Session session) {
        session.execute(KEYSPACE_DELETION_QUERY);
    }

    public void initDefaultKeyspace(Session session) {
//        session.execute(KEYSPACE_DELETION_QUERY);
        session.execute(KEYSPACE_CREATION_QUERY);
    }

    private QueryOptions getConsistencyLevel() {
        if (CONSISTENCY_LVL.compareTo("ONE") == 0)
            return new QueryOptions().setConsistencyLevel(ConsistencyLevel.ONE);
        if (CONSISTENCY_LVL.compareTo("QUORUM") == 0)
            return new QueryOptions().setConsistencyLevel(ConsistencyLevel.QUORUM);
        if (CONSISTENCY_LVL.compareTo("SERIAL") == 0)
            return new QueryOptions().setConsistencyLevel(ConsistencyLevel.LOCAL_SERIAL);
        return null;
    }

}
