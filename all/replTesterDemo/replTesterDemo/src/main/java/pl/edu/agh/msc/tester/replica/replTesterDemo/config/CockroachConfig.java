package pl.edu.agh.msc.tester.replica.replTesterDemo.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CockroachConfig {

    private static final String DATABASE_NAME = LoadProperty.getString("cockroach.database_name");
    private static final String USER_NAME = "root";
    private static final String USER_PASS = "";

    private static final String HOST_ADDRESS = LoadProperty.getString("cockroach.data_center_host");
    private static final String NODE2_ADDRESS = "149.156.11.4";
    private static final String NODE3_ADDRESS = "149.156.11.4";
    private static final int SEED_PORT = LoadProperty.getInt("cockroach.port1");
    private static final int NODE2_PORT = LoadProperty.getInt("cockroach.port2");
    private static final int NODE3_PORT = LoadProperty.getInt("cockroach.port3");

    private static final String URL_CONNECTION_FORMAT = "jdbc:postgresql://%s:%s/";

    private static Logger logger = LogManager.getLogger(CockroachConfig.class);


    public CockroachConfig() throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
    }

    public List<Connection> getCluster() throws SQLException {

        String urlConn1 = String.format(URL_CONNECTION_FORMAT, HOST_ADDRESS, SEED_PORT);
        String urlConn2 = String.format(URL_CONNECTION_FORMAT, NODE2_ADDRESS, NODE2_PORT);
        String urlConn3 = String.format(URL_CONNECTION_FORMAT, NODE3_ADDRESS, NODE3_PORT);
        logger.debug("Formated url: " + urlConn1);

        Connection connection1 = DriverManager.getConnection(urlConn1, USER_NAME, USER_PASS);
        Connection connection2 = DriverManager.getConnection(urlConn2, USER_NAME, USER_PASS);
        Connection connection3 = DriverManager.getConnection(urlConn3, USER_NAME, USER_PASS);

        String query = "CREATE DATABASE IF NOT EXISTS " + DATABASE_NAME + ";";
        connection1.createStatement().execute(query);

        List<Connection> connectionList = new ArrayList();
        connectionList.add(connection1);
        connectionList.add(connection2); // TODO: support loadbalancing better, pls.
        connectionList.add(connection3); // TODO: because sometimes it is needed for tests, in different time no. Should be more convenient.
        return connectionList;
    }
}
