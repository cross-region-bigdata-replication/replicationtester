package pl.edu.agh.msc.tester.replica.replTesterDemo.config;

import java.util.Properties;

/**
 * Created by Maciej Makowka on 11.05.2018.
 */
public class SystemConfig {

    private Properties properties;

    public SystemConfig() {
        properties = System.getProperties();

        setAsynchronousLogging();
    }

    private void setAsynchronousLogging() {
        properties.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
    }
}
