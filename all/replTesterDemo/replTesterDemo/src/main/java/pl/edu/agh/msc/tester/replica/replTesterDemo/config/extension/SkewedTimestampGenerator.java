package pl.edu.agh.msc.tester.replica.replTesterDemo.config.extension;

import com.datastax.driver.core.TimestampGenerator;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.TimestampHelper;

import java.util.Random;

/**
 * Created by Maciej Makowka on 20.05.2018.
 */
public class SkewedTimestampGenerator implements TimestampGenerator {

    private static final int BOUNDARY = 100;

    @Override
    public long next() {
        int skew;
        Random random = new Random();
        skew = random.nextInt(BOUNDARY) - BOUNDARY/2;
        long timestamp = TimestampHelper.getCurrentTimestamp().getTime() + skew;
//        System.out.println(timestamp);
        return timestamp;
    }
}
