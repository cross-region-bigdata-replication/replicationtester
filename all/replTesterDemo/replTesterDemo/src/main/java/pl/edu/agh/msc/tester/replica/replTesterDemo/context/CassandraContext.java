package pl.edu.agh.msc.tester.replica.replTesterDemo.context;

import com.datastax.driver.core.*;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.config.CassandraConfig;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CassandraContext implements IDbContext {

    private static final Logger logger = LoggerFactory.getLogger(CassandraContext.class);

    private final String KEYSPACE_NAME = "testKeySpace";
    private List<Session> sessions;
    private CassandraConfig cassandraConfig;
    private final boolean ROUND_ROBIN = LoadProperty.getBoolean("cassandra.round_robin");

    public CassandraContext() {
        cassandraConfig = new CassandraConfig();
        sessions = establishSessions(cassandraConfig);
        cassandraConfig.initDefaultKeyspace(getSession());
    }

    public void cleanDefaultKeyspace() {
        cassandraConfig.deleteDefaultKeyspace(getSession());
        cassandraConfig.initDefaultKeyspace(getSession());
    }


    public ResultSet executeNonIdempotentQuery(String query) {
        Statement statement = new SimpleStatement(query);
        statement.setIdempotent(false);
//        statement.setConsistencyLevel(ConsistencyLevel.ALL);
        return getSession().execute(statement);
    }

    public ResultSet executeNonIdempotentQuorumQuery(String query) {
        Statement statement = new SimpleStatement(query);
        statement.setIdempotent(false);
        statement.setConsistencyLevel(ConsistencyLevel.ALL);
        return getSession().execute(statement);
    }

    public void executeNonIdempotentQuerySingleAsync(String query) {
        new Thread(() -> {
            Statement statement = new SimpleStatement(query);
            statement.setIdempotent(false);
            getSession().execute(statement);
        }).start();
    }

    public void executeNonIdempotentQueryDoubleAsync(String query) {
        new Thread(() -> {
            Statement statement = new SimpleStatement(query);
            statement.setIdempotent(false);
            getSession().executeAsync(statement);
        }).start();
    }

    public ResultSet executeIdempotentQuery(String query) {
        Statement statement = new SimpleStatement(query);
        statement.setIdempotent(true);
        return getSession().execute(statement);
    }

    public void executeIdempotentQueryAsync(String query) {
        new Thread(() -> {
            Statement statement = new SimpleStatement(query);
            statement.setIdempotent(true);
            getSession().executeAsync(statement);
        });
    }

    @Override
    public void insertQueryDefault(String query) {
//        executeNonIdempotentQuerySingleAsync(query);
        executeNonIdempotentQuery(query);
    }

    @Override
    public void deleteQueryDefault(String query) {
        executeIdempotentQueryAsync(query);
    }

    public boolean closeSession() {
        sessions.forEach(Session::close);
        return sessions.stream().allMatch(Session::isClosed);
    }

    private Session getSession() {
        int amount = sessions.size();
        Random rand = new Random();
        int randomNumber = rand.nextInt(amount);
        Session session = sessions.get(randomNumber);
        if (!ROUND_ROBIN)
            session = sessions.get(1);
        logger.debug(session.getCluster().getClusterName());
        return session;
    }

    private List<Session> establishSessions(CassandraConfig cassandraConfig) {

        Cluster cluster1 = cassandraConfig.getCluster();
        Cluster cluster2 = cassandraConfig.getCluster();
        Cluster cluster3 = cassandraConfig.getCluster();

        sessions = new ArrayList<>();

        Session session1 = cluster1.connect();
        Session session2 = cluster2.connect();
        Session session3 = cluster3.connect();

        sessions.add(session1);
        sessions.add(session2);
        sessions.add(session3);

        return sessions;
    }

}
