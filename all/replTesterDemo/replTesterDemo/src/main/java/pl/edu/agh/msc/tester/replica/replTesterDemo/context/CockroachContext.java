package pl.edu.agh.msc.tester.replica.replTesterDemo.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.config.CockroachConfig;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Random;

public class CockroachContext implements IDbContext {

    private static final Logger logger = LoggerFactory.getLogger(CockroachContext.class);

    private List<Connection> connections;

    public CockroachContext() throws ClassNotFoundException, SQLException {
        CockroachConfig cockroachConfig = new CockroachConfig();
        connections = cockroachConfig.getCluster();
    }

    public boolean executeDDLQuery(String query) throws SQLException {
        // CREATE, DROP, ALTER
        return getConnection().createStatement().execute(query);
    }

    public boolean executeInsertQuery(String query) throws SQLException {
        // INSERT
        Statement statement = getConnection().createStatement();
        return statement.execute(query);
    }

    public void executeInsertQueryAsync(String query) {
        new Thread(() -> {
            try {
                getConnection().createStatement().execute(query);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public ResultSet executeSelectQuery(String query) throws SQLException {
        // SELECT only
        return getConnection().createStatement().executeQuery(query);
    }

    public void executeSelectQueryAsync(String query) throws SQLException {
        // SELECT only
        new Thread(() -> {
            try {
                getConnection().createStatement().executeQuery(query);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void executeDeleteQuery(String query) throws SQLException {
        getConnection().createStatement().execute(query);
    }

    public int executeUpdateQuery(String query) throws SQLException {
        return getConnection().createStatement().executeUpdate(query);
    }

    public void executeDeleteQueryAsync(String query) throws SQLException {
        new Thread(() -> {
            try {
                getConnection().createStatement().execute(query);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();
    }


    @Override
    public void insertQueryDefault(String query) {
        try {
            executeInsertQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteQueryDefault(String query) {
        try {
            executeDeleteQueryAsync(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean closeConnection() {
        connections.forEach(c -> {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return connections.stream().allMatch(c -> {
            try {
                return c.isClosed();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    private Connection getConnection() {
        int amount = connections.size();
        Random rand = new Random();
        int randomNumber = rand.nextInt(amount);
//        System.out.println(connections.get(randomNumber).toString());
        return connections.get(randomNumber);
    }

}
