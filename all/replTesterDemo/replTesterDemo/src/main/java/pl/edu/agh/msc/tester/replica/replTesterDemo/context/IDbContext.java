package pl.edu.agh.msc.tester.replica.replTesterDemo.context;

public interface IDbContext {

    void insertQueryDefault(String query);
    void deleteQueryDefault(String query);
}
