package pl.edu.agh.msc.tester.replica.replTesterDemo.generator;

import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Created by Maciej Makowka on 09.07.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.helper
 * Time 12:42
 */
public class BigDataGenerator {

    private static final String TABLE_NAME = LoadProperty.getString("generator.table_name1");

    private static final String OUTPUT_FILE_NAME = "generated_data.sql";
    private static final int LASTNAME_LENGTH = LoadProperty.getInt("test.bigdatagen.lastname_length");
    private static final int FIRSTNAME_LENGTH = LoadProperty.getInt("test.bigdatagen.firstname_length");
    private static final int CITY_LENGTH = LoadProperty.getInt("test.bigdatagen.city_length");
    private static final int EMAIL_LENGTH = LoadProperty.getInt("test.bigdatagen.email_length");

    private PrintWriter writer;

    public BigDataGenerator() throws FileNotFoundException, UnsupportedEncodingException {
        writer = new PrintWriter(OUTPUT_FILE_NAME, "UTF-8");
    }

    public void generateSql(int rowQuantity) {
        for (int i = 0; i < rowQuantity-1; i++) {
            Users user = RandomUser.getRandomUser(LASTNAME_LENGTH, CITY_LENGTH, EMAIL_LENGTH, FIRSTNAME_LENGTH);
            writer.print(getQueryFromUser(user) + "\n");
            writer.flush();
        }
        writer.println("---END---");

        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getQueryFromUser(Users user) {
        String heading = Users.whatKindInsertColumn();
        String whatValue = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        return GeneralQueries.insertRow(TABLE_NAME, heading, whatValue);
    }
}
