package pl.edu.agh.msc.tester.replica.replTesterDemo.generator;

import pl.edu.agh.msc.tester.replica.replTesterDemo.context.IDbContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Maciej Makowka on 09.07.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.helper
 * Time 12:41
 */
public class BigDataLoader {

    private final IDbContext context;
    private static final String OUTPUT_FILE_NAME = "generated_data.sql";
    private FileInputStream fis = null;
    private Scanner sc = null;

    public BigDataLoader(IDbContext context) {
        this.context = context;
    }

    public void executeQueriesFromFile(int startFromLine) throws FileNotFoundException, InterruptedException {
        fis = new FileInputStream(OUTPUT_FILE_NAME);
        sc = new Scanner(fis, "UTF-8");
        int i = 0;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (i >= startFromLine) {
                context.insertQueryDefault(line);
            }
            i++;
        }
    }
}
