package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Request;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Vertex;

import java.util.*;

/**
 * Created by Maciej Makowka on 08.06.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.helper
 * Time 14:22
 * https://stackoverflow.com/questions/19757371/directed-graph-in-java
 */
public class DiGraph {

    private static final double INF = Double.POSITIVE_INFINITY;

    // Map is used to map each destVertex to its list of adjacent vertices.
    private Map<Vertex, List<Edge<Vertex>>> neighbours = new HashMap<>();

    private int no_edges;

    public DiGraph() {

    }

    // Edge implementation
    public static class Edge<Vertex> {
        private Vertex destVertex;
        private Vertex srcVertex;
        private int cost;

        public Edge(Vertex srcVertex, Vertex destVertex, int cost) {
            this.srcVertex = srcVertex;
            this.destVertex = destVertex;
            this.cost = cost;
        }

        public Vertex getDestVertex() {
            return destVertex;
        }

        public Vertex getSrcVertex() {
            return srcVertex;
        }

        public int getCost() {
            return cost;
        }

        @Override
        public String toString() {
            return "Edge{" +
                    "destVertex=" + destVertex +
                    ", srcVertex=" + srcVertex +
                    ", cost=" + cost +
                    '}';
        }
    }

    // Adding destVertex to the graph. Nothing happens if destVertex is already in graph.
    public Vertex addVertex(Vertex vertex) {
        if (neighbours.containsKey(vertex))
            return null;
        neighbours.put(vertex, new ArrayList<>());
        return vertex;
    }

    public Vertex getVertex(Request readRequest) {
        for (Vertex v : neighbours.keySet()) {
            if (v.getRequest().equals(readRequest))
                return v;
        }
        return null;
    }

    public void removeVertex(Vertex readVertex) {
        // remove edges to destVertex
        for (Vertex to : neighbours.keySet()) {
            List<Edge<Vertex>> edges = neighbours.get(to);
            List<Edge> edgesToRemove = new LinkedList<>();
            if (edges != null) {
                for (Edge e : edges) {
                    if (e.getSrcVertex().equals(readVertex) || e.getDestVertex().equals(readVertex))
                        edgesToRemove.add(e);
                }
            }
            if (edges != null) {
                edges.removeAll(edgesToRemove);
            }
        }
        // remove destVertex
        neighbours.keySet().remove(readVertex);
//        System.out.println("REMOVE: " + readVertex);
    }

    public List<Vertex> verticles() {
        List<Vertex> vertices = new LinkedList<>(neighbours.keySet());
        return vertices;
    }

    public List<Edge<Vertex>> getOutEdges(Vertex vertex) {
        return neighbours.get(vertex);
    }

    public List<Edge<Vertex>> getInEdges(Vertex vertex) {
        List<Edge<Vertex>> inList = new ArrayList<>();
        if (neighbours.keySet() != null)
            for (Vertex to : neighbours.keySet())
                if (neighbours.get(to) != null)
                    for (Edge<Vertex> e : neighbours.get(to))
                        if (e.destVertex.equals(vertex))
                            inList.add(e);
        return inList;
    }

    public int getEdgesNo() {
        int sum = 0;
        for (List<Edge<Vertex>> e : neighbours.values()) {
            sum += e.size();
        }
        return sum;
    }

    public boolean contains(Vertex vertex) {
        return neighbours.containsKey(vertex);
    }

    public void addEdge(Vertex from, Vertex to, int cost) {
        if (from.equals(to))
            return;
        this.addVertex(from);
        this.addVertex(to);
        neighbours.get(from).add(new Edge<>(from, to, cost));
        neighbours.get(to).add(new Edge<>(from, to, cost));
    }

    public int outDegree(Vertex vertex) {
        return neighbours.get(vertex).size();
    }

    public int inDegree(Vertex vertex) {
        return inboundNeighbours(vertex).size();
    }

    public List<Vertex> breadthFirstSearch(Vertex vertex) {

        List<Vertex> visitedVertex = new LinkedList<>();

        Map<Vertex, Integer> colors = new HashMap<>();
        Map<Vertex, Double> distance = new HashMap<>();
        Map<Vertex, Vertex> parent = new HashMap<>();

        for (Vertex tV : neighbours.keySet()) {
            colors.put(tV, 0);
            distance.put(tV, INF);
            parent.put(tV, null);
        }

        colors.put(vertex, 1);
        distance.put(vertex, 0.0);
        parent.put(vertex, null);

        Queue q = new PriorityQueue();
        q.add(vertex);
        while (!q.isEmpty()) {
            Vertex u = (Vertex) q.remove();
            for (Edge<Vertex> e : neighbours.get(u)) {
                Vertex neighbour = e.destVertex;
                if (colors.get(neighbour) == 0) {
                    colors.replace(neighbour, 1);
                    distance.replace(neighbour, distance.get(neighbour) + 1);
                    parent.replace(neighbour, u);
                    q.add(neighbour);
                    if (!visitedVertex.contains(neighbour))
                        visitedVertex.add(neighbour);
                }
            }
            colors.replace(u, 2);
        }

        return visitedVertex;
    }

    public boolean checkForCycles() {

        Map<Vertex, Boolean> recStack = new HashMap<>();
        Map<Vertex, Boolean> visited = new HashMap<>();

        // initialize all vertices as not visited and not in recursive stack
        for (Vertex v : verticles()) {
            visited.put(v, false);
            recStack.put(v, false);
        }

        return isCyclicUtil(verticles().get(0), visited, recStack);
    }

    private List<Vertex> inboundNeighbours(Vertex inboundVertex) {
        List<Vertex> inList = new ArrayList<>();
        for (Vertex to : neighbours.keySet())
            for (Edge e : neighbours.get(to))
                if (e.destVertex.equals(inboundVertex))
                    inList.add(to);
        return inList;
    }

    private boolean isEdge(Vertex from, Vertex to) {
        for (Edge<Vertex> e : neighbours.get(from)) {
            if (e.destVertex.equals(to))
                return true;
        }
        return false;
    }

    private int getCost(Vertex from, Vertex to) {
        for (Edge<Vertex> e : neighbours.get(from))
            if (e.destVertex.equals(to))
                return e.cost;
        return -1;
    }

    private Vertex getParent(Vertex children) {
        List<Vertex> inList = new ArrayList<>();
        for (Vertex node : neighbours.keySet()) {
            for (Edge e : neighbours.get(node)) {
                if (e.destVertex.equals(children))
                    return node;
            }
        }
        return null;
    }

    private List<Vertex> getChildren(Vertex v) {
        List<Vertex> outList = new ArrayList<>();
        List<Edge<Vertex>> outEdges = getOutEdges(v);
        if (outEdges != null)
            for (Edge<Vertex> e : outEdges) {
                if (e.getSrcVertex().equals(v))
                    outList.add(e.getDestVertex());
            }
        return outList;
    }

    // https://www.geeksforgeeks.org/detect-cycle-in-a-graph/
    private boolean isCyclicUtil(Vertex v, Map<Vertex, Boolean> visited, Map<Vertex, Boolean> recStack) {

        // Make the current node as visited and part of recursion stack
        if (recStack.get(v)) {
            return true;
        }
        if (visited.get(v))
            return false;

        visited.replace(v, true);
        recStack.replace(v, true);
        List<Vertex> children = getChildren(v);

        for (Vertex child : children)
            if (isCyclicUtil(child, visited, recStack))
                return true;
        recStack.replace(v, false);

        return false;
    }


    // String representation of the graph
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Vertex Vertex : neighbours.keySet())
            s.append("\n    ").append(Vertex).append(" -> ").append(neighbours.get(Vertex));
        return s.toString();
    }

}
