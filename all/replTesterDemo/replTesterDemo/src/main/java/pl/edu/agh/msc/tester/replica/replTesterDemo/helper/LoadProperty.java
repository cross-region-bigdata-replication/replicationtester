package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Maciej Makowka on 01.06.2018.
 * Package name is ${PACKAGE_NAME}
 * Time 14:52
 */
public class LoadProperty {

    private static File file = new File("./");
    private static URL[] urls;
    static {
        try {
            urls = new URL[]{file.toURI().toURL()};
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    private static ClassLoader loader = new URLClassLoader(urls);

    private static ResourceBundle rb = ResourceBundle.getBundle("config", Locale.getDefault(), loader);

    public LoadProperty() throws MalformedURLException {
    }

    public static boolean getBoolean(String propertyName) {
        return Boolean.parseBoolean(rb.getString(propertyName));
    }

    public static int getInt(String propertyName) {
        return Integer.parseInt(rb.getString(propertyName));
    }

    public static String getString(String propertyName) {
        return rb.getString(propertyName);
    }

}
