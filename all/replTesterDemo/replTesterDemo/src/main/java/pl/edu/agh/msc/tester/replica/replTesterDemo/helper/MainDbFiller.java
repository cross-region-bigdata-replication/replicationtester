package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import com.datastax.driver.core.exceptions.OperationTimedOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.LinearizabilityChecker;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Request;
import pl.edu.agh.msc.tester.replica.replTesterDemo.config.SystemConfig;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.generator.BigDataGenerator;
import pl.edu.agh.msc.tester.replica.replTesterDemo.generator.BigDataLoader;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

//@SpringBootApplication
public class MainDbFiller {

    private static Logger logger = LoggerFactory.getLogger(MainDbFiller.class);

    private static final boolean GENERATE_NEW_DATA = LoadProperty.getBoolean("test.all.generate_new_data");
    private static final boolean LOAD_NEW_DATA_COCKROACH = LoadProperty.getBoolean("test.cockroach.load_new_data");
    private static final boolean LOAD_NEW_DATA_CASSANDRA = LoadProperty.getBoolean("test.cassandra.load_new_data");

    public static void main(String[] args) {
        SystemConfig systemConfig = new SystemConfig();
//		SpringApplication.run(MainDbFiller.class, args);

        DiGraph graph = new DiGraph();
        List<Request> requests = getSampleRequests();
        LinearizabilityChecker lc = new LinearizabilityChecker(graph);
//		lc.linearizableCheck(requests);

        System.out.println("---Database generator, refiller---\nPut the amount of rows to fill");
        Scanner sc = new Scanner(System.in);
        int amount = sc.nextInt();

        if (GENERATE_NEW_DATA) {
            try {
                BigDataGenerator bigDataGenerator = new BigDataGenerator();
                bigDataGenerator.generateSql(amount);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (LOAD_NEW_DATA_COCKROACH) {
            try {
                CockroachContext cockroachContext = new CockroachContext();
                BigDataLoader bigDataLoader = new BigDataLoader(cockroachContext);
                bigDataLoader.executeQueriesFromFile(0);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException | InterruptedException | FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (LOAD_NEW_DATA_CASSANDRA) {
            try {
                CassandraContext cassandraContext = new CassandraContext();
                BigDataLoader bigDataLoader = new BigDataLoader(cassandraContext);
                bigDataLoader.executeQueriesFromFile(0);
            } catch (FileNotFoundException | InterruptedException | OperationTimedOutException e) {
                e.printStackTrace();
            }
        }

        //################################################ CASSANDRA #############################################

        //################################################ COCKROACH #############################################

    }

    private static List<Request> getSampleRequests() {
        Request r1 = new Request(new UUID(1, 2), false, "*");
        r1.setArg(1);
        r1.setRespT(new Timestamp(10));
        r1.setInvocT(new Timestamp(8));

        Request r2 = new Request(new UUID(1, 2), false, "*");
        r2.setArg(2);
        r2.setRespT(new Timestamp(10));
        r2.setInvocT(new Timestamp(8));

        Request w1 = new Request(new UUID(1, 2), true, "*");
        w1.setArg(1);
        w1.setRespT(new Timestamp(4));
        w1.setInvocT(new Timestamp(2));

        Request w2 = new Request(new UUID(1, 2), true, "*");
        w2.setArg(2);
        w2.setRespT(new Timestamp(7));
        w2.setInvocT(new Timestamp(5));

        List<Request> requests = new LinkedList<>();
        requests.add(r1);
//		requests.add(r2);
        requests.add(w1);
        requests.add(w2);

        return requests;
    }
}
