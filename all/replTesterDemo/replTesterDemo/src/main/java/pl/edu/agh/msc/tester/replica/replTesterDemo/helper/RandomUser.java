package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;

import java.util.Random;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class RandomUser {

    private static final int MAX_AGE = LoadProperty.getInt("test.bigdatagen.age_max");

    public RandomUser() {
    }

    public static Users getRandomUser() {
        RandomString randomString = new RandomString();
        int randomAge = getRandomAge();

        return new Users(
                randomString.nextString(),
                randomAge,
                randomString.nextString(),
                randomString.nextString(),
                randomString.nextString()
        );
    }

    public static Users getRandomUser(int lastnameLength, int cityLength, int emailLength, int firstnameLength) {
        return new Users(
                new RandomString(lastnameLength).nextString(),
                getRandomAge(),
                new RandomString(cityLength).nextString(),
                new RandomString(emailLength).nextString(),
                new RandomString(firstnameLength).nextString()
        );
    }

    public static int getRandomAge() {
        Random random = new Random();
        return random.nextInt(MAX_AGE);
    }
}
