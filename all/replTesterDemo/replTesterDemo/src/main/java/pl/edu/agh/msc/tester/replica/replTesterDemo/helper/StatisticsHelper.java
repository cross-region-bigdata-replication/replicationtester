package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model.TimestampModel;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

public class StatisticsHelper {

    private static final Logger logger = LoggerFactory.getLogger(StatisticsHelper.class);

    private final String statisticName;

    private long measureNo = 0;
    private long lastTimestamp = 0;
    private StatisticInputTuple sInTuple;

    public StatisticsHelper(String statisticName) {
        this.statisticName = statisticName;
    }

    public XYDataset createDataSet(List<TimestampModel> timestampModels) {
        final XYSeriesCollection dataSet = new XYSeriesCollection();

        int items;
        double sum;
        double[] data;

        for (TimestampModel tm : timestampModels) {
            setLastTimestamp(tm.getTimestamp().get(0).getTime());
            XYSeries series = new XYSeries(tm.getLabel());
            items = 0;
            sum = 0;
            data = new double[tm.getTimestamp().size()];
            for (Timestamp tms : tm.getTimestamp()) {
                double duration = tms.getTime() - getLastTimestamp();
                series.add(updateMeasureNo(), duration);
                setLastTimestamp(tms.getTime());
                data[items] = duration;
                sum += duration;
                items++;
            }
            dataSet.addSeries(series);
            sInTuple = new StatisticInputTuple();
            sInTuple.sum = sum;
            sInTuple.items = items;
            sInTuple.data = data;
        }

        return dataSet;
    }

    public void getStatistics(List<TimestampModel> timestampModels) {
        XYDataset dataSet = createDataSet(timestampModels);
        if (sInTuple != null) {
            calculateStatistics(sInTuple.sum, sInTuple.items, sInTuple.data);
        } else {
            logger.error("Input tuple should have not been empty");
        }
        dataSet = null;
    }

    public void getDetailedStatistics(List<TimestampModel> timestampModels) {
        XYDataset dataSet = createDataSet(timestampModels);
        Arrays.stream(sInTuple.data).forEach(System.out::println);
        if (sInTuple != null) {
            calculateStatistics(sInTuple.sum, sInTuple.items, sInTuple.data);
        } else {
            logger.error("Input tuple should have not been empty");
        }
        dataSet = null;
    }


    private void calculateStatistics(double sum, int items, double[] data) {
        double mean;
        double median;
        double variance;
        double std;

        // Mean
        mean = getMean(sum, items);

        // Median
        median = getMedian(data, items);

        // Variance
        variance = getVariance(data, mean, items);

        // Std
        std = getStd(variance);

        logger.error(String.format("\n### %s \nSum: %f; Mean: %f; Median: %f; Variance: %f; Std: %f\n###\n",
                statisticName, sum, mean, median, variance, std));
    }

    private double getMean(double sum, int items) {
        return sum / items;
    }

    private double getMedian(double[] data, int items) {
        Arrays.sort(data, 0, items);
        if (items % 2 == 0) {
            return (data[(items / 2) - 1] + data[items / 2]) / 2.0;
        }
        return data[items / 2];
    }

    private double getVariance(double[] data, double mean, int items) {
        double temp = 0;
        for (double a : data)
            temp += (a - mean) * (a + mean);
        return temp / (items - 1);
    }

    private double getStd(double variance) {
        return Math.sqrt(variance);
    }

    private long updateMeasureNo() {
        measureNo++;
        return measureNo;
    }

    private long getLastTimestamp() {
        return lastTimestamp;
    }

    private void setLastTimestamp(long timestamp) {
        lastTimestamp = timestamp;
    }

    private static class StatisticInputTuple {
        double sum;
        int items;
        double[] data;
    }

}
