package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.TimestampChart;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model.TimestampModel;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class Timer {

    private final String timerLabel;
    private boolean started;
    private boolean executed;

    private List<Timestamp> timestamps;
    private List<TimestampModel> timestampModels;
    private TimestampModel timestampModel;

    public Timer(String timerLabel) {
        this.timerLabel = timerLabel;
        this.started = false;
        this.executed = false;
    }

    public void start() {
        timestamps = new ArrayList<>();
        timestampModels = new ArrayList<>();
        timestampModel = new TimestampModel(timerLabel);
        timestamps.add(TimestampHelper.getCurrentTimestamp());
        started = true;
    }

    public void add(Timestamp timestamp) {
        if (started && !executed) {
            timestamps.add(timestamp);
        } else {

        }
    }

    public void stop() {
        if (started) {
            timestamps.add(TimestampHelper.getCurrentTimestamp());
            timestampModel.setTimestamp(timestamps);
            timestampModels.add(timestampModel);
            executed = true;
        } else {

        }
    }

    public void getResults(){
        if (executed){

        } else {

        }
    }

    public void drawChart() throws IOException {
        if (executed) {
            TimestampChart.run(timerLabel, timestampModels);
        } else {

        }
    }

    public String getTimerLabel() {
        return timerLabel;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isExecuted() {
        return executed;
    }

    public List<Timestamp> getTimestamps() {
        return timestamps;
    }

    public List<TimestampModel> getTimestampModels() {
        return timestampModels;
    }

    public TimestampModel getTimestampModel() {
        return timestampModel;
    }
}
