package pl.edu.agh.msc.tester.replica.replTesterDemo.helper;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampHelper {

    public TimestampHelper() {
    }

    public static Timestamp getCurrentTimestamp() {
        Date today = new Date();
        return new Timestamp(today.getTime());
    }

    public static long diff(Date start, Date stop) {
        if (start.compareTo(stop) > 0) {
            java.util.Date tmp = start;
            start = stop;
            stop = tmp;
        }
        return stop.getTime() - start.getTime();
    }
}
