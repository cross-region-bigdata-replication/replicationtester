package pl.edu.agh.msc.tester.replica.replTesterDemo.model;

public class Users {

    private String lastname;

    private int age;

    private String city;

    private String email;

    private String firstname;


    public Users(String lastname, int age, String city, String email, String firstname) {
        this.lastname = lastname;
        this.age = age;
        this.city = city;
        this.email = email;
        this.firstname = firstname;
    }

    public static String getVarcharColumnDefinition() {
        return "lastname TEXT PRIMARY KEY, age SMALLINT, " +
                "city TEXT, email TEXT, " +
                "firstname TEXT";
    }

    public static String getCassandraColumnDefinition() {
        return "lastname TEXT PRIMARY KEY, age SMALLINT, " +
                "city TEXT, email TEXT, " +
                "firstname TEXT";
    }

    public static String whatKindInsertColumn() {
        return "lastname, age, city, email, firstname";
    }

    public static String getColumnValueOrder() {
        return "lastname, age, city, email, firstname";
    }

    public String getLastname() {
        return lastname;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public String toString() {
        return "Users{" +
                "lastname='" + lastname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                ", email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }
}
