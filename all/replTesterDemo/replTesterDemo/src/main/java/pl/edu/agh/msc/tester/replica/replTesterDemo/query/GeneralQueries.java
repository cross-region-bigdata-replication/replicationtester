package pl.edu.agh.msc.tester.replica.replTesterDemo.query;

public class GeneralQueries {
    //TODO: get queries from resources files, even if general split up on the cassandra/cockroach/etc set.

    public static String selectFrom(String what, String table) {
        return String.format("SELECT %s FROM %s ;", what, table);
    }

    public static String selectFrom(String what, String table, String where) {
        return String.format("SELECT %s FROM %s WHERE %s ;", what, table, where);
    }

    public static String createTable(String tableName, String columns) {
        return String.format("CREATE TABLE IF NOT EXISTS %s ( %s );", tableName, columns);
    }

    public static String dropTable(String tableName) {
        return String.format("DROP TABLE IF EXISTS %s ;", tableName);
    }

    public static String insertRow(String tableName, String whatKind, String whatValue) {
        return String.format("INSERT INTO %s ( %s ) VALUES ( %s ) ;", tableName, whatKind, whatValue);
    }

    public static String updateRow(String tableName, String whatToSet, String whereCondition) {
        return String.format("UPDATE %s SET %s WHERE %s ;", tableName, whatToSet, whereCondition);
    }

    public static String deleteRow(String tableName, String whereCondition) {
        return String.format("DELETE FROM %s WHERE %s ;", tableName, whereCondition);
    }

    public static String deleteRow(String tableName) {
        return String.format("DELETE FROM %s;", tableName);
    }

    public static String truncateTable(String tableName) {
        return String.format("TRUNCATE TABLE %s ;", tableName);
    }
}
