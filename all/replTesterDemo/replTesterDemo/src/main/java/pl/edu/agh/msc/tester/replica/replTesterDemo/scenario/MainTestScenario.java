package pl.edu.agh.msc.tester.replica.replTesterDemo.scenario;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.config.SystemConfig;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra.*;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach.*;

public class MainTestScenario {

    private static Logger logger = LogManager.getLogger(MainTestScenario.class);

    private static final boolean PUBLISH_CHARTS = LoadProperty.getBoolean("test.charts");

    public static void main(String[] args) {
        SystemConfig systemConfig = new SystemConfig();

        try {

            final boolean DELETION_CASSANDRA = LoadProperty.getBoolean("test.cassandra.delete1");
            if (DELETION_CASSANDRA) {
                DeletionTestCassandra deletionTestCassandra = new DeletionTestCassandra("Cassandra deletion test", 1, 100);
                deletionTestCassandra.run("100");
                waitTillExecuted(deletionTestCassandra);
                if (PUBLISH_CHARTS)
                    deletionTestCassandra.publishChart();
                deletionTestCassandra.getStatistics(false);
            }

            final boolean DELETION_COCKROACH = LoadProperty.getBoolean("test.cockroach.delete1");
            if (DELETION_COCKROACH) {
                DeletionTestCockroach deletionTestCockroach = new DeletionTestCockroach("Cockroach deletion test", 1, 100);
                deletionTestCockroach.run("100");
                waitTillExecuted(deletionTestCockroach);
                if (PUBLISH_CHARTS)
                    deletionTestCockroach.publishChart();
                deletionTestCockroach.getStatistics(false);
            }

            final boolean FIRST_CONSISTENCY_COCKROACH = LoadProperty.getBoolean("test.cockroach.consistency1");
            if (FIRST_CONSISTENCY_COCKROACH) {
                FirstConsistencyTestCockroach consistencyTestCockroach = new FirstConsistencyTestCockroach("Cockroach consistency test", 1, 100);
                consistencyTestCockroach.run("100");
                waitTillExecuted(consistencyTestCockroach);
                if (PUBLISH_CHARTS)
                    consistencyTestCockroach.publishChart();
            }

            final boolean FIRST_CONSISTENCY_CASSANDRA = LoadProperty.getBoolean("test.cassandra.consistency1");
            if (FIRST_CONSISTENCY_CASSANDRA) {
                FirstConsistencyTestCassandra consistencyTestCassandra = new FirstConsistencyTestCassandra("Cassandra consistency test", 1, 100);
                consistencyTestCassandra.run("100");
                waitTillExecuted(consistencyTestCassandra);
                if (PUBLISH_CHARTS)
                    consistencyTestCassandra.publishChart();
            }

            final boolean SELECT1_COCKROACH = LoadProperty.getBoolean("test.cockroach.select1");
            if (SELECT1_COCKROACH) {
                SelectTestCockroach selectTestCockroach = new SelectTestCockroach("Cockroach select test", 1, 100);
                selectTestCockroach.run("100");
                waitTillExecuted(selectTestCockroach);
                if (PUBLISH_CHARTS)
                    selectTestCockroach.publishChart();
                selectTestCockroach.getStatistics(false);
            }

            final boolean SELECT1_CASSANDRA = LoadProperty.getBoolean("test.cassandra.select1");
            if (SELECT1_CASSANDRA) {
                SelectTestCassandra selectTestCassandra = new SelectTestCassandra("Cassandra select test", 1, 100);
                selectTestCassandra.run("100");
                waitTillExecuted(selectTestCassandra);
                if (PUBLISH_CHARTS)
                    selectTestCassandra.publishChart();
                selectTestCassandra.getStatistics(false);
            }

            final boolean INSERT1_COCKROACH = LoadProperty.getBoolean("test.cockroach.insert1");
            final int INSERT1_COCKROACH_NOE = LoadProperty.getInt("test.insert.noe");
            for (int i = 0; i < INSERT1_COCKROACH_NOE; i++) {
                if (INSERT1_COCKROACH) {
                    logger.error("Insert1 execution number: " + i);
                    final String INSERT_NOR_COCKROACH = LoadProperty.getString("test.insert.nor");
                    InsertTestCockroach insertTestCockroach = new InsertTestCockroach("Cockroach insert test", 1, 100);
                    insertTestCockroach.run(INSERT_NOR_COCKROACH);
                    waitTillExecuted(insertTestCockroach);
                    if (PUBLISH_CHARTS)
                        insertTestCockroach.publishChart();
                    insertTestCockroach.getStatistics(true);
                }
            }

            final int INSERT1_CASSANDRA_NOE = LoadProperty.getInt("test.insert.noe");
            for (int i = 0; i < INSERT1_CASSANDRA_NOE; i++) {
                final boolean INSERT1_CASSANDRA = LoadProperty.getBoolean("test.cassandra.insert1");
                if (INSERT1_CASSANDRA) {
                    logger.error("Insert1 execution number: " + i);
                    final String INSERT_NOR_CASSANDRA = LoadProperty.getString("test.insert.nor");
                    InsertTestCassandra insertTestCassandra = new InsertTestCassandra("Cassandra insert test", 1, 100);
                    insertTestCassandra.run(INSERT_NOR_CASSANDRA);
                    waitTillExecuted(insertTestCassandra);
                    if (PUBLISH_CHARTS)
                        insertTestCassandra.publishChart();
                    insertTestCassandra.getStatistics(true);
                }
            }

            final boolean MIXED_COCKROACH = LoadProperty.getBoolean("test.cockroach.mixed");
            if (MIXED_COCKROACH) {
                MixedTestCockroach mixedTestCockroach = new MixedTestCockroach("Cockroach mixed test", 1, 100);
                mixedTestCockroach.run("100");
                waitTillExecuted(mixedTestCockroach);
                if (PUBLISH_CHARTS)
                    mixedTestCockroach.publishChart();
                mixedTestCockroach.getStatistics(false);
            }

            final boolean MIXED_CASSANDRA = LoadProperty.getBoolean("test.cassandra.mixed");
            if (MIXED_CASSANDRA) {
                MixedTestCassandra mixedTestCassandra = new MixedTestCassandra("Cassandra mixed test", 1, 100);
                mixedTestCassandra.run("100");
                waitTillExecuted(mixedTestCassandra);
                if (PUBLISH_CHARTS)
                    mixedTestCassandra.publishChart();
                mixedTestCassandra.getStatistics(false);
            }

            final boolean SEQUENTIAL_COCKROACH = LoadProperty.getBoolean("test.cockroach.sequential");
            if (SEQUENTIAL_COCKROACH) {
                SequentialTestCockroach sequentialTestCockroach = new SequentialTestCockroach("Cockroach sequential test", 1, 100);
                sequentialTestCockroach.run("5");
                waitTillExecuted(sequentialTestCockroach);
                if (PUBLISH_CHARTS)
                    sequentialTestCockroach.publishChart();
            }

            final boolean SEQUENTIAL_CASSANDRA = LoadProperty.getBoolean("test.cassandra.sequential");
            if (SEQUENTIAL_CASSANDRA) {
                SequentialTestCassandra sequentialTestCassandra = new SequentialTestCassandra("Cassandra sequential test", 1, 100);
                sequentialTestCassandra.run("5");
                waitTillExecuted(sequentialTestCassandra);
                if (PUBLISH_CHARTS)
                    sequentialTestCassandra.publishChart();
            }

            final boolean EVENTUAL_CONSISTENCY_CASSANDRA = LoadProperty.getBoolean("test.cassandra.eventualconsistency");
            if (EVENTUAL_CONSISTENCY_CASSANDRA) {
                EventualConsistencyTestCassandra eventualConsistencyTestCassandra = new EventualConsistencyTestCassandra("Cassandra eventual consistency test", 1, 100);
                eventualConsistencyTestCassandra.run("10");
                waitTillExecuted(eventualConsistencyTestCassandra);
                if (PUBLISH_CHARTS)
                    eventualConsistencyTestCassandra.publishChart();
            }

            final boolean EVENTUAL_CONSISTENCY_COCKROACH = LoadProperty.getBoolean("test.cockroach.eventualconsistency");
            if (EVENTUAL_CONSISTENCY_COCKROACH) {
                EventualConsistencyTestCockroach eventualConsistencyTestCockroach = new EventualConsistencyTestCockroach("CockroachDB eventual consistency test", 1, 100);
                eventualConsistencyTestCockroach.run("10");
                waitTillExecuted(eventualConsistencyTestCockroach);
                if (PUBLISH_CHARTS)
                    eventualConsistencyTestCockroach.publishChart();
            }

//            ResultPrinter rp1 = new ResultPrinter("Cassandra Strong 500", eventualConsistencyTestCassandra.isPositive());
//            ResultPrinter rp2 = new ResultPrinter("CockroachDB Strong 500", eventualConsistencyTestCockroach.isPositive());
//            ResultPrinter rp3 = new ResultPrinter("Cassandra Sequential 5", sequentialTestCassandra.isPositive());
//            ResultPrinter rp4 = new ResultPrinter("Cockroach Sequential 5", sequentialTestCockroach.isPositive());
//            logger.info(rp1.printResultLine() + rp2.printResultLine() + rp3.printResultLine() + rp4.printResultLine());

            final boolean SELECT_BIGDATA_CASSANDRA = LoadProperty.getBoolean("test.cassandra.bigdata1");
            if (SELECT_BIGDATA_CASSANDRA) {
                SelectBigData1TestCassandra selectBigData1TestCassandra = new SelectBigData1TestCassandra("Cassandra select big data count test", 1, 100);
                selectBigData1TestCassandra.run("100");
                waitTillExecuted(selectBigData1TestCassandra);
                if (PUBLISH_CHARTS)
                    selectBigData1TestCassandra.publishChart();
                selectBigData1TestCassandra.getStatistics(false);
            }

            final boolean SELECT_BIGDATA_COCKROACH = LoadProperty.getBoolean("test.cockroach.bigdata1");
            if (SELECT_BIGDATA_COCKROACH) {
                SelectBigData1TestCockroach selectBigData1TestCockroach = new SelectBigData1TestCockroach("Cockroach select big data count test", 1, 100);
                selectBigData1TestCockroach.run("100");
                waitTillExecuted(selectBigData1TestCockroach);
                if (PUBLISH_CHARTS)
                    selectBigData1TestCockroach.publishChart();
                selectBigData1TestCockroach.getStatistics(false);
            }

            final String LINEAR_NOE = LoadProperty.getString("test.linear.noe");
            final boolean LINEARIZABLE_CASSANDRA = LoadProperty.getBoolean("test.cassandra.linearizable");
            if (LINEARIZABLE_CASSANDRA) {
                LinearizabilityConsistencyTestCassandra linearizabilityConsistencyTestCassandra = new LinearizabilityConsistencyTestCassandra("Cassandra linearizability data test", 1, 100);
                linearizabilityConsistencyTestCassandra.run(LINEAR_NOE);
                waitTillExecuted(linearizabilityConsistencyTestCassandra);
                if (PUBLISH_CHARTS)
                    linearizabilityConsistencyTestCassandra.publishChart();
                linearizabilityConsistencyTestCassandra.getStatistics(false);
            }

            final boolean LINEARIZABLE_COCKROACH = LoadProperty.getBoolean("test.cockroach.linearizable");
            if (LINEARIZABLE_COCKROACH) {
                LinearizabilityConsistencyTestCockroach linearizabilityConsistencyTestCockroach = new LinearizabilityConsistencyTestCockroach("Cockroach linearizability data test", 1, 100);
                linearizabilityConsistencyTestCockroach.run(LINEAR_NOE);
                waitTillExecuted(linearizabilityConsistencyTestCockroach);
                if (PUBLISH_CHARTS)
                    linearizabilityConsistencyTestCockroach.publishChart();
                linearizabilityConsistencyTestCockroach.getStatistics(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void waitTillExecuted(DbSingleTest test) {
        while (!test.isExecuted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
