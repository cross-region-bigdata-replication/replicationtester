package pl.edu.agh.msc.tester.replica.replTesterDemo.scenario.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomString;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.Timer;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.TimestampHelper;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.TimestampChart;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model.TimestampModel;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CassandraTestScenario {

    private static Logger logger = LogManager.getLogger(CassandraTestScenario.class);

    private CassandraContext cassandraContext;

    public CassandraTestScenario() {
        cassandraContext = new CassandraContext();
        // TODO: check whether database and table exists
        cassandraContext.cleanDefaultKeyspace();
        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable("testKeySpace.users", columns));
    }

}
