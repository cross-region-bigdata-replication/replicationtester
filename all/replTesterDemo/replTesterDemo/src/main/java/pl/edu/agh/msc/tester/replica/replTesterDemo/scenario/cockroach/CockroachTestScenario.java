package pl.edu.agh.msc.tester.replica.replTesterDemo.scenario.cockroach;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomString;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.Timer;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.TimestampHelper;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.TimestampChart;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model.TimestampModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CockroachTestScenario {

    private static Logger logger = LogManager.getLogger(CockroachTestScenario.class);

    private CockroachContext cockroachContext;

    public CockroachTestScenario() throws SQLException, ClassNotFoundException {
        cockroachContext = new CockroachContext();
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.dropTable("users"));
        cockroachContext.executeDDLQuery(GeneralQueries.createTable("users", columns));
    }

}
