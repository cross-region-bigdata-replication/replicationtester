package pl.edu.agh.msc.tester.replica.replTesterDemo.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.*;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public abstract class DbSingleTest {

    private final String testName;

    private int testTimeout;
    private int testRepetition;
    private Timer timer;
    private boolean executed;

    private static final Logger logger = LoggerFactory.getLogger(DbSingleTest.class);
    private static final int LASTNAME_LENGTH = LoadProperty.getInt("test.bigdatagen.lastname_length");
    private static final int FIRSTNAME_LENGTH = LoadProperty.getInt("test.bigdatagen.firstname_length");
    private static final int CITY_LENGTH = LoadProperty.getInt("test.bigdatagen.city_length");
    private static final int EMAIL_LENGTH = LoadProperty.getInt("test.bigdatagen.email_length");


    public DbSingleTest(String testName, int testRepetition, int testTimeout) throws Exception {
        this.testName = testName;
        this.testRepetition = testRepetition;
        this.testTimeout = testTimeout;
        this.executed = false;
    }

    public void run(String args) throws Exception {
        prepareConnection();
        prepareTest("100");

        timer = new Timer(testName);
        new Thread(() -> {
            logger.info(this.getClass().getName() + " :: test :: " + "START");
            timer.start();
            try {
                performTest(args);
            } catch (Exception e) {
                logger.error(String.format("testName: %s; message: %s\n%s\n", testName, e.getMessage(), e.getCause()) + Arrays.toString(e.getStackTrace()));
            }
            timer.stop();
            executed = true;
            logger.info(this.getClass().getName() + " :: test :: " + "FINISH");
        }).start();
    }

    public boolean isExecuted() {
        return executed;
    }

    public abstract boolean isPositive();

    public void publishChart() throws IOException {
        timer.drawChart();
    }

    public void getStatistics(boolean detailed) {
        StatisticsHelper statisticsHelper = new StatisticsHelper(getTestName());
        if (detailed)
            statisticsHelper.getDetailedStatistics(timer.getTimestampModels());
        else
            statisticsHelper.getStatistics(timer.getTimestampModels());
    }

    protected void addTime() {
        timer.add(TimestampHelper.getCurrentTimestamp());
    }

    protected Logger getLogger() {
        return logger;
    }

    protected String getTestName() {
        return testName;
    }

    protected Users getRandomUser(boolean propertiesRowSizeModel) {
        if (propertiesRowSizeModel)
            return RandomUser.getRandomUser(LASTNAME_LENGTH, CITY_LENGTH, EMAIL_LENGTH, FIRSTNAME_LENGTH);
        return RandomUser.getRandomUser();
    }

    protected abstract void prepareConnection() throws Exception;

    protected abstract void prepareTest(String args) throws Exception; // TODO

    protected abstract void performTest(String args) throws Exception;

}
