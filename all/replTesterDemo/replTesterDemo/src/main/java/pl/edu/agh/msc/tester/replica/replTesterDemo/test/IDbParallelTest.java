package pl.edu.agh.msc.tester.replica.replTesterDemo.test;

import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;

import java.sql.SQLException;
import java.util.concurrent.*;

/**
 * Created by Maciej Makowka on 01.06.2018.
 * Package name is ${PACKAGE_NAME}
 * Time 20:54
 */
public interface IDbParallelTest {

    default boolean doParallelTask() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Callable<Boolean> task1 = doCallableTask();
        Callable<Boolean> task2 = doCallableTask();
        Callable<Boolean> task3 = doCallableTask();
        Callable<Boolean> task4 = doCallableTask();
        Callable<Boolean> task5 = doCallableTask();
        Callable<Boolean> task6 = doCallableTask();
        Callable<Boolean> task7 = doCallableTask();
        Callable<Boolean> task8 = doCallableTask();
        Callable<Boolean> task9 = doCallableTask();
        Callable<Boolean> task10 = doCallableTask();

        Future<Boolean> future1 = executorService.submit(task1);
        Future<Boolean> future2 = executorService.submit(task2);
        Future<Boolean> future3 = executorService.submit(task3);
        Future<Boolean> future4 = executorService.submit(task4);
        Future<Boolean> future5 = executorService.submit(task5);
        Future<Boolean> future6 = executorService.submit(task6);
        Future<Boolean> future7 = executorService.submit(task7);
        Future<Boolean> future8 = executorService.submit(task8);
        Future<Boolean> future9 = executorService.submit(task9);
        Future<Boolean> future10 = executorService.submit(task10);


        executorService.shutdown();

        while (!future1.isDone() || !future2.isDone() || !future3.isDone() || !future4.isDone() || !future5.isDone() || !future6.isDone() || !future7.isDone() || !future8.isDone() || !future9.isDone() || !future10.isDone()) {
            //Thread.sleep(1); // TODO: if no sleep, then very high CPU usage in Java Management Console seen.
        }

        return future1.get() && future2.get() && future3.get() && future4.get() && future5.get() && future6.get() && future7.get() && future8.get() && future9.get() && future10.get();
    }

    default boolean checkNodeContains(Users user) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Callable<Boolean> task1 = getCallableResult(user);
        Callable<Boolean> task2 = getCallableResult(user);
        Callable<Boolean> task3 = getCallableResult(user);

        Future<Boolean> future1 = executorService.submit(task1);
        Future<Boolean> future2 = executorService.submit(task2);
        Future<Boolean> future3 = executorService.submit(task3);
        executorService.shutdown();
        while (!future1.isDone() || !future2.isDone() || !future3.isDone()) {
            //Thread.sleep(1); // TODO: if no sleep, then very high CPU usage in Java Management Console seen.
        } // TODO: check if it can be

        return future1.get() && future2.get() && future3.get();
    }

    Callable<Boolean> getCallableResult(Users user);

    Callable<Boolean> doCallableTask() throws SQLException, Exception;

}
