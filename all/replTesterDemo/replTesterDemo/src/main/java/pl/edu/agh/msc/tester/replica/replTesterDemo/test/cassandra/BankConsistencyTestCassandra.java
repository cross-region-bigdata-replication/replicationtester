package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

public class BankConsistencyTestCassandra extends DbSingleTestCassandra {

    private static Logger logger;

    private CassandraContext cassandraContext;

    public BankConsistencyTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
//
//        String columns = Users.getCassandraColumnDefinition();
//        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable("testKeySpace.users", columns));
//        Thread.sleep(1000);
//
//        int i = 10; //Integer.parseInt(args);
//        Users user;
//
//        for (int j=0; j < i; j++) {
//            user = getRandomUser();
//            basicInsertTest(user);
//        }
//        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {

    }

}
