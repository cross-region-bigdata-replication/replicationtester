package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

/**
 * Created by Maciej Makowka on 01.06.2018.
 */
public abstract class DbSingleTestCassandra extends DbSingleTest {

    private final String KEYSPACE_NAME = LoadProperty.getString("cassandra.keyspace_name");
    private final String DATA_TABLE_NAME = LoadProperty.getString("cassandra.table_name1");
    private final boolean CLEAR_TABLE = LoadProperty.getBoolean("test.clear_table");
    private final String TABLE_NAME = String.format("%s.%s", KEYSPACE_NAME, DATA_TABLE_NAME);

    private CassandraContext cassandraContext;

    public DbSingleTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        cassandraContext = getCassandraContext();
    }

    protected String getKEYSPACE_NAME() {
        return KEYSPACE_NAME;
    }

    protected String getDATA_TABLE_NAME() {
        return DATA_TABLE_NAME;
    }

    public String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public CassandraContext getCassandraContext() {
        if (cassandraContext == null)
            cassandraContext = new CassandraContext();
        return cassandraContext;
    }

    @Override
    protected void prepareConnection() throws Exception {
        // TODO: check whether database and table exists
        String tableName = getTABLE_NAME();
        String columns = Users.getCassandraColumnDefinition();
        if (CLEAR_TABLE) {
            getCassandraContext().cleanDefaultKeyspace();
            getCassandraContext().executeIdempotentQuery(GeneralQueries.dropTable(tableName));
            getCassandraContext().executeIdempotentQuery(GeneralQueries.createTable(tableName, columns));
        }
        Thread.sleep(1000);
    }

    protected void insertOperation(Users user) {
        String whatKind = Users.getColumnValueOrder();
        String whatValue = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String query = GeneralQueries.insertRow(getTABLE_NAME(), whatKind, whatValue);
        getLogger().debug(String.format("INSERT: %s\n", query));
        getCassandraContext().executeNonIdempotentQuorumQuery(query);
    }
}
