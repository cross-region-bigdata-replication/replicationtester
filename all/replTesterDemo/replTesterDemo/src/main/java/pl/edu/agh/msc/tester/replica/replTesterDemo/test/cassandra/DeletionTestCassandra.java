package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

public class DeletionTestCassandra extends DbSingleTestCassandra {


    private final String TABLE_NAME = getTABLE_NAME();
    private static Logger logger;

    private CassandraContext cassandraContext;

    public DeletionTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        int i = 100; //Integer.parseInt(args);
        Users user;

        for (int j = 0; j < i; j++) {
            user = getRandomUser(true);
            insertOperation(user);
        }
        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < 1; i++) {
            basicDeleteTest(1);
            logger.debug("Test no: " + i + " executed");

            addTime();
        }
        logger.info(":ok");
    }

    private void basicDeleteTest(int i) {
        String query = GeneralQueries.truncateTable(TABLE_NAME);
        cassandraContext.executeIdempotentQuery(query);
    }
}
