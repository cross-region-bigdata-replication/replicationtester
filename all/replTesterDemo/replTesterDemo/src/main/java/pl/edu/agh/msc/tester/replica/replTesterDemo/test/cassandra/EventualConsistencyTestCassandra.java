package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.IDbParallelTest;

import java.util.concurrent.*;

public class EventualConsistencyTestCassandra extends DbSingleTestCassandra implements IDbParallelTest {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CassandraContext cassandraContext;
    private boolean isPositive = true;

    private int iterations = 0;
    private int errors = 0;

    public EventualConsistencyTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {

        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < selectCount; i++) {
            iterations++;
            Users user = getRandomUser(true);
            insertOperation(user);
            verifyResult(user);
            addTime();
            logger.debug("Test no: " + i + " executed");
        }
        logger.error("Executed with error rate: " + errors + " / " + iterations);
        logger.info(":ok");
    }

    private void verifyResult(Users user) throws ExecutionException, InterruptedException {
        boolean contains = true;
        if (!checkNodeContains(user))
            contains = false;

        if (!contains) {
            logger.error(":Wrong");
            isPositive = false;
            errors++;
        } else
            logger.debug(":Ok");
    }

    private ResultSet selectOperation() {
        return cassandraContext
                .executeIdempotentQuery(GeneralQueries.selectFrom("*", TABLE_NAME));
    }

    public Callable<Boolean> getCallableResult(Users user) {
        return () -> {
            ResultSet resultSet = selectOperation();
            return resultSetContains(resultSet, user);
        };
    }

    @Override
    public Callable<Boolean> doCallableTask() {
        return null;
    }

    private boolean resultSetContains(ResultSet resultSet, Users user) {
        boolean found = false;
        for (Row row : resultSet) {
            if (row.getString(0).equals(user.getLastname()))
                found = true;
        }
        if (!found) {
            logger.error(String.valueOf(resultSet.getExecutionInfo()));
            logger.error(String.valueOf(resultSet.wasApplied()));
        }
        return found;
    }

}
