package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomString;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class FirstConsistencyTestCassandra extends DbSingleTestCassandra {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private boolean isPositive = true;
    private CassandraContext cassandraContext;

    public FirstConsistencyTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        int i = Integer.parseInt(args);
    }

    @Override
    public void performTest(String args) {
        int iterations = Integer.parseInt(args);
        int performedIterations = 0;

        Users user = getRandomUser(true);
        String whatKind = Users.getColumnValueOrder();
        String whatValue = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String insertQuery = GeneralQueries.insertRow(TABLE_NAME, whatKind, whatValue);
        cassandraContext.executeNonIdempotentQuery(insertQuery);


        String selectQuery = GeneralQueries.selectFrom("count(*)", TABLE_NAME, "lastname = '" + user.getLastname() + "'");
        ResultSet selectResult = cassandraContext.executeIdempotentQuery(selectQuery);

        if (selectResult.one().getLong(0) != 1) {
            logger.error("In database should be exactly one users with name " + user.getLastname() + " , " +
                    "but exists " + selectResult.one().getLong(0) + ". Validate this error with consistency.");
        }

        int tmp = 0;
        RandomString randomString = new RandomString();

        while (tmp < iterations) {

            // update
            String newFirstName = randomString.nextString();
            String updateQuery = GeneralQueries.updateRow(TABLE_NAME, "firstname = '"
                    + newFirstName + "'", "lastname = '" + user.getLastname() + "'");
            cassandraContext.executeNonIdempotentQuery(updateQuery);

            boolean found = false;
            int subIteration = 0;
            while ((!found) && (subIteration < 2)) {
                // select
                selectQuery = GeneralQueries.selectFrom("firstname", TABLE_NAME, "lastname = '" + user.getLastname() + "'");
                selectResult = cassandraContext.executeIdempotentQuery(selectQuery);
                // check
                Row row = selectResult.one();
                if (row != null && row.getString(0) != null) {
                    String newFirstNameBase = row.getString(0);
                    if (!newFirstNameBase.equalsIgnoreCase(newFirstName)) {
                        logger.error("In database should be user with name " + user.getLastname() + "  " +
                                "with first name: " + newFirstName + ", but his first name is: " + newFirstNameBase + ". Validate this error with consistency.");
                        isPositive = false;
                    } else {
                        found = true;
                        logger.debug("Success in iteration no: " + tmp + ", and subiteration no: " + subIteration);
                        performedIterations++;
                        addTime();
                    }
                } else {
                    logger.error("In database node should be user with name " + user.getLastname() + "  " +
                            "SelectResult, SelectResult Row or Select Result Row String is null. Validate this error with consistency.");
                }

                subIteration++;
            }
            tmp++;
        }

        if (performedIterations == iterations)
            logger.info(":ok");
        else
            logger.error(":wrong");
    }

}
