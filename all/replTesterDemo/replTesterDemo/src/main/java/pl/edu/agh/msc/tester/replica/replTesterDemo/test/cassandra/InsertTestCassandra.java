package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

/**
 * Created by Maciej Makowka on 20.05.2018.
 */
public class InsertTestCassandra extends DbSingleTestCassandra {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CassandraContext cassandraContext;
    private long rowsAmount;
    private boolean isPositive = false;

    public InsertTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }


    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        rowsAmount = getAllRows("count(*)", TABLE_NAME).one().getLong(0);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int insertCount = Integer.parseInt(args);

        for (int i = 0; i < insertCount; i++) {
            Users user = getRandomUser(true);
            insertOperation(user);
            logger.debug("Test no: " + i + " executed");
            addTime();
        }

        verifyResult(insertCount);
    }

    private void verifyResult(int insertCount) {
        ResultSet cassandraRs = getAllRows("count(*)", TABLE_NAME);
        long insertedCount = cassandraRs.one().getLong(0);

        if (insertCount != (insertedCount - rowsAmount))
            logger.error(":Wrong");
        else {
            logger.info(":Ok");
            isPositive = true;
        }
    }

    private ResultSet getAllRows(String column, String table) {
        return cassandraContext.executeIdempotentQuery(GeneralQueries.selectFrom(column, table));
    }

}
