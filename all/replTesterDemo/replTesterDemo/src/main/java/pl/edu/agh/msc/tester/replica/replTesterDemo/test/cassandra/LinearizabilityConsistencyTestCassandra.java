package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.LinearizabilityChecker;
import pl.edu.agh.msc.tester.replica.replTesterDemo.checker.model.Request;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.DiGraph;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.TimestampHelper;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.IDbParallelTest;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Maciej Makowka on 05.09.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra
 * Time 21:17
 */
public class LinearizabilityConsistencyTestCassandra extends DbSingleTestCassandra implements IDbParallelTest {

    private static final int NO_RANDOM_USER = LoadProperty.getInt("test.linear.users");
    private static final int NO_EXECUTORS = LoadProperty.getInt("test.linear.executors");
    private static Logger logger;

    private final String TABLE_NAME = getTABLE_NAME();

    private CassandraContext cassandraContext;
    private DiGraph graph;
    private LinearizabilityChecker linearizabilityChecker;
    private List<Request> requestList;
    private List<Users> users;
    private boolean isPositive = false;

    public LinearizabilityConsistencyTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        graph = new DiGraph();
        users = new ArrayList<>();
        requestList = Collections.synchronizedList(new ArrayList<>());
        linearizabilityChecker = new LinearizabilityChecker(graph);

        String columns = Users.getVarcharColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        // insert 50 example rows
        for (int i = 0; i < NO_RANDOM_USER; i++) {
            Users user = getRandomUser(true);
            insertOperation(user);
            users.add(user);
        }

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        Callable<Boolean> doActionCallable = this::doAction;

        List<Callable<Boolean>> taskList = new ArrayList<>();
        for (int i = 0; i < selectCount; i++) {
            taskList.add(doActionCallable);
            addTime();
        }

        ExecutorService executorService = Executors.newFixedThreadPool(NO_EXECUTORS);

        List<Future<Boolean>> executorResults = executorService.invokeAll(taskList);
        executorResults.forEach(r -> {
            boolean result = false;
            try {
                while (!r.isDone()) {
                    Thread.sleep(1);
                }
                result = r.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error(Arrays.toString(e.getStackTrace()));
                logger.error(e.getMessage());
            }
        });

        logger.info(":checker");
        int linearizableErrors = linearizabilityChecker.linearizableCheck(requestList);
        logger.error("linearizable errors: " + linearizableErrors + "/" + selectCount);
        logger.info(":ok");
        if (linearizableErrors == 0)
            isPositive = true;
    }

    private Boolean doAction() throws Exception {
        int arg;
        int randOp;
        int randU;
        String query;
        boolean write;
        Timestamp invocT;
        Timestamp respT;
        Users randomUser;
        Random random = new Random();

        randU = random.nextInt(NO_RANDOM_USER);
        randomUser = users.get(randU);
        randOp = random.nextInt(100);
        if (randOp < 70) {
            //READ
            write = false;
            query = GeneralQueries.selectFrom(
                    "age",
                    TABLE_NAME,
                    "lastname='" + randomUser.getLastname() + "'"
            );
            invocT = TimestampHelper.getCurrentTimestamp();
            ResultSet rs = cassandraContext.executeIdempotentQuery(query);
            respT = TimestampHelper.getCurrentTimestamp();
            arg = 0;
            Row row = rs.one();
            if (row != null)
                arg = row.getShort(0);
        } else {
            //WRITE
            write = true;
            int newAge = random.nextInt(RandomUser.getRandomAge());
            query = GeneralQueries.updateRow(
                    TABLE_NAME,
                    "age=" + newAge + "",
                    "lastname='" + randomUser.getLastname() + "'"
            );
            invocT = TimestampHelper.getCurrentTimestamp();
            ResultSet rs = cassandraContext.executeNonIdempotentQuorumQuery(query);
            Row row = rs.one();
            respT = TimestampHelper.getCurrentTimestamp();
            arg = newAge;
        }

        Request request = new Request(UUID.randomUUID(), write, query);
        request.setArg(arg);
        request.setInvocT(invocT);
        request.setRespT(respT);

        addRequestToList(request);
//        System.out.println(request);
        Random rand = new Random();
        Thread.sleep(rand.nextInt(1000));
        return true;
    }

    private void addRequestToList(Request request) {
        requestList.add(request);
    }

    @Override
    public Callable<Boolean> getCallableResult(Users user) {
        return null;
    }

    @Override
    public Callable<Boolean> doCallableTask() throws SQLException, Exception {
        return null;
    }
}
