package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.TypeCodec;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.IDbParallelTest;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by Maciej Makowka on 02.09.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra
 * Time 15:43
 */
public class MixedTestCassandra extends DbSingleTestCassandra implements IDbParallelTest {

    private static Logger logger;

    private final String TABLE_NAME = getTABLE_NAME();
    private final int NO_OPS = LoadProperty.getInt("test.mixed.noo");
    private final double PROB_S = Double.parseDouble(LoadProperty.getString("test.mixed.prob.s"));
    private final double PROB_I = Double.parseDouble(LoadProperty.getString("test.mixed.prob.i"));
    private final double PROB_U = Double.parseDouble(LoadProperty.getString("test.mixed.prob.u"));

    private final double MIN_S = 0;
    private final double MAX_S = 100 * PROB_S;
    private final double MIN_I = 100 * PROB_S;
    private final double MAX_I = 100 * (PROB_S + PROB_I);
    private final double MIN_U = 100 * (PROB_S + PROB_I);
    private final double MAX_U = 1;

    private Users victim;

    private CassandraContext cassandraContext;

    public MixedTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        victim = doInsert();
        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        for (int i = 0; i < NO_OPS; i++) {
            doParallelTask();
        }
    }

    private void doUpdate() {
        Users user = getRandomUser(true);
        cassandraContext
                .executeNonIdempotentQuery(
                        GeneralQueries.updateRow(
                                TABLE_NAME,
                                String.format("city='%s'", user.getCity()),
                                String.format("lastname='%s'", victim.getLastname())
                        )
                );
    }

    private Users doInsert() {
        Users user = getRandomUser(true);
        insertOperation(user);
        victim = user;
        return user;
    }

    private void doSelect() {
        Random random = new Random();
        int randomInt = random.nextInt(99);

        ResultSet cassandraRs = cassandraContext
                .executeIdempotentQuery(
                        GeneralQueries.selectFrom(
                                "age",
                                TABLE_NAME,
                                String.format("lastname='%s'", victim.getLastname()))
                );
//        logger.debug(String.format("age = %d -> count = %d;", randomInt, cassandraRs.one().get(0, TypeCodec.bigint())));
    }

    @Override
    public Callable<Boolean> getCallableResult(Users user) {
        return null;
    }

    @Override
    public Callable<Boolean> doCallableTask() throws Exception {
        return () -> {
            int randomInt;
            Random random = new Random();
            randomInt = random.nextInt(100);
            if (randomInt <= MAX_S) {
                doSelect();
            } else if (randomInt <= MAX_I) {
                doInsert();
            } else {
                doUpdate();
            }
            addTime();
            return true;
        };
    }
}
