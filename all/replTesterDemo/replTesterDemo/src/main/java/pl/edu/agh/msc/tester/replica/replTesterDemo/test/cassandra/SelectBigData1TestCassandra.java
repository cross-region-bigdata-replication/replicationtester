package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.TypeCodec;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

/**
 * Created by Maciej Makowka on 09.07.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra
 * Time 20:20
 */
public class SelectBigData1TestCassandra extends DbSingleTestCassandra {

    private static final int MULTIPLY_ELEMENTS = 1;
    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CassandraContext cassandraContext;

    public SelectBigData1TestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }


    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < selectCount; i++) {
            selectCountWithCondition(1, i);
            addTime();
            logger.debug("Test no: " + i + " executed");
        }
        logger.info(":ok");
    }

    private void selectCountWithCondition(int selectCount, int arg) {
        for (int i = 0; i < selectCount; i++) {
            ResultSet cassandraRs = cassandraContext
                    .executeIdempotentQuery(
                            GeneralQueries.selectFrom(
                                    "count(*)",
                                    TABLE_NAME,
                                    String.format("age=%d", arg))
                                    .replace(";", " ALLOW FILTERING;")
                    );
            //TODO: print result
            logger.debug(String.format("age = %d -> count = %d;", arg, cassandraRs.one().get(0, TypeCodec.bigint())));
        }
    }
}
