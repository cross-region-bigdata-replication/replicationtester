package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 20.05.2018.
 */
public class SelectTestCassandra extends DbSingleTestCassandra {

    private static final int MULTIPLY_ELEMENTS = 1;
    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CassandraContext cassandraContext;

    public SelectTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {

        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        int i = 100; //Integer.parseInt(args);
        Users user;

        for (int j = 0; j < i; j++) {
            user = getRandomUser(true);
            insertOperation(user);
        }
        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);
//        prepareTest(selectCount * MULTIPLY_ELEMENTS);

        for (int i = 0; i < selectCount; i++) {
            basicSelectTest(1);
            addTime();
            logger.debug("Test no: " + i + " executed");
        }
        logger.info(":ok");
    }

    private void basicSelectTest(int selectCount) {
        for (int i = 0; i < selectCount; i++) {
            ResultSet cassandraRs = cassandraContext
                    .executeIdempotentQuery(GeneralQueries.selectFrom("*", TABLE_NAME));

            for (Row row : cassandraRs) {
                logger.debug(String.format("%s \n", row.getString("firstname")));
            }
        }
    }

}
