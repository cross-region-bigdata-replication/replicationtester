package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cassandra;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

/**
 * Created by Maciej Makowka on 19.05.2018.
 */
public class SequentialTestCassandra extends DbSingleTestCassandra {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME(); //TODO
    private final String KEYSPACE_NAME = LoadProperty.getString("cassandra.keyspace_name");
    private final boolean FIRST_ASYNC = LoadProperty.getBoolean("test.seq.first_async");
    private final String TABLE_X = String.format("%s.%s", KEYSPACE_NAME, "usersX");
    private final String TABLE_Y = String.format("%s.%s", KEYSPACE_NAME, "usersY");

    private boolean isPositive = true;
    private CassandraContext cassandraContext;

    private int iterations = 0;
    private int errors = 0;

    public SequentialTestCassandra(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cassandraContext = getCassandraContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareConnection() throws Exception {
        // TODO: check whether database and table exists
        String columns = Users.getCassandraColumnDefinition();
        cassandraContext.cleanDefaultKeyspace();
        cassandraContext.executeIdempotentQuery(GeneralQueries.dropTable(TABLE_X));
        cassandraContext.executeIdempotentQuery(GeneralQueries.dropTable(TABLE_Y));
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_X, columns));
        cassandraContext.executeIdempotentQuery(GeneralQueries.createTable(TABLE_Y, columns));
        Thread.sleep(1000);
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        int i = Integer.parseInt(args);
    }

    @Override
    protected void performTest(String args) throws Exception {
        int insertCount = 50;

        for (int i = 0; i < insertCount; i++) {
            addTime();
            Users user = getRandomUser(true);
            sequentialInsertTest(user);
            logger.debug(String.format("%d. :: Test performed", i));
            addTime();
            iterations++;

            cassandraContext.executeIdempotentQuery(GeneralQueries.truncateTable(TABLE_X));
            cassandraContext.executeIdempotentQuery(GeneralQueries.truncateTable(TABLE_Y));
        }

        logger.error("Executed with error rate: " + errors + " / " + iterations);
        logger.info(":ok");
    }

    private void sequentialInsertTest(Users user) {

        new Thread(this::selectData).start();

        new Thread(() -> insertData(user)).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void selectData() {
        String countTableXQuery = GeneralQueries.selectFrom("count(*)", TABLE_X);
        String countTableYQuery = GeneralQueries.selectFrom("count(*)", TABLE_Y);

        ResultSet resultX;
        Row rowX;
        ResultSet resultY = cassandraContext.executeNonIdempotentQuery(countTableYQuery);
        Row rowY = resultY.one();
        while (rowY.getLong(0) != 1) {
            resultY = cassandraContext.executeNonIdempotentQuery(countTableYQuery);
            rowY = resultY.one();
        }
        resultX = cassandraContext.executeNonIdempotentQuery(countTableXQuery);
        rowX = resultX.one();
        if (rowX.getLong(0) == 1) {
            logger.info(":Ok");
        } else {
            logger.error(":Wrong");
            isPositive = false;
            logger.error("result y= " + rowY.getLong(0) + "; result x= " + rowX.getLong(0));
            logger.error(resultY + " ; " + resultX);
            errors++;

            int i = 1;
            while (rowX.getLong(0) < 1 && i < 10) {
                resultX = cassandraContext.executeNonIdempotentQuery(countTableXQuery);
                rowX = resultX.one();
                logger.error("result y= " + rowY.getLong(0) + "; result x= " + rowX.getLong(0));
                i++;
            }
            if (i < 10) {
                logger.error(":Ok result after " + i + " next iterations.");
            } else {
                logger.error(":Wrong result ends test after" + i + " iterations.");
            }
        }
    }

    private void insertData(Users user) {
        String whatKindX = Users.getColumnValueOrder();
        String whatValueX = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String queryX = GeneralQueries.insertRow(TABLE_X, whatKindX, whatValueX);

        String whatKindY = Users.getColumnValueOrder();
        String whatValueY = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String queryY = GeneralQueries.insertRow(TABLE_Y, whatKindY, whatValueY);

        if (FIRST_ASYNC) {
            cassandraContext.executeNonIdempotentQueryDoubleAsync(queryX);
            logger.error("Async");
        }
        else {
            cassandraContext.executeNonIdempotentQuery(queryX);
            logger.error("NotAsync");
        }
        cassandraContext.executeNonIdempotentQueryDoubleAsync(queryY);
    }

}
