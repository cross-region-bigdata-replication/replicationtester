package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 01.06.2018.
 */
public abstract class DbSingleTestCockroach extends DbSingleTest {

    private final String DB_NAME = LoadProperty.getString("cockroach.database_name");
    private final String DATA_TABLE_NAME = LoadProperty.getString("cockroach.table_name1");
    private final String TABLE_NAME = String.format("%s.%s", DB_NAME, DATA_TABLE_NAME);
    private final boolean CLEAR_TABLE = LoadProperty.getBoolean("test.clear_table");

    private CockroachContext cockroachContext;

    public DbSingleTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        cockroachContext = getCockroachContext();
    }

    public String getTABLE_NAME() {
        return TABLE_NAME;
    }

    protected CockroachContext getCockroachContext() throws SQLException, ClassNotFoundException {
        if (cockroachContext == null)
            cockroachContext = new CockroachContext();
        return cockroachContext;
    }

    @Override
    protected void prepareConnection() throws Exception {
        cockroachContext = new CockroachContext();
        String columns = Users.getVarcharColumnDefinition();
        if (CLEAR_TABLE) {
            cockroachContext.executeDDLQuery(GeneralQueries.dropTable(getTABLE_NAME()));
            cockroachContext.executeDDLQuery(GeneralQueries.createTable(getTABLE_NAME(), columns));
        }
        Thread.sleep(1000);
    }

    protected void insertOperation(Users user) throws SQLException, ClassNotFoundException {
        String whatKind = Users.getColumnValueOrder();
        String whatValue = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String query = GeneralQueries.insertRow(TABLE_NAME, whatKind, whatValue);
        getLogger().debug(String.format("INSERT: %s\n", query));
        getCockroachContext().executeInsertQuery(query);
    }

}
