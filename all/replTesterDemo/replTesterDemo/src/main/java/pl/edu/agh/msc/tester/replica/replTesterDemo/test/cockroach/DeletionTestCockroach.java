package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.SQLException;
import java.util.Random;

public class DeletionTestCockroach extends DbSingleTestCockroach {


    private final String TABLE_NAME = getTABLE_NAME();
    private static Logger logger;

    private CockroachContext cockroachContext;

    private Random random;

    public DeletionTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
        random = new Random();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        int i = 100; //Integer.parseInt(args);
        Users user;

        for (int j = 0; j < i; j++) {
            user = getRandomUser(true);
            insertOperation(user);
        }
        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < 1; i++) {
            try {
                basicDeleteTest(1);
                logger.debug("Test no: " + i + " executed");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            addTime();
        }
        logger.info(":ok");
    }

    private void basicDeleteTest(int i) throws SQLException {
        String query = GeneralQueries.truncateTable(TABLE_NAME);
        cockroachContext.executeDeleteQuery(query);
    }


}
