package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.IDbParallelTest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.*;

public class EventualConsistencyTestCockroach extends DbSingleTestCockroach implements IDbParallelTest {

    private final String TABLE_NAME = getTABLE_NAME();

    private static Logger logger;

    private CockroachContext cockroachContext;
    private boolean isPositive = true;

    private int iterations = 0;
    private int errors = 0;

    public EventualConsistencyTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < selectCount; i++) {
            iterations++;
            Users user = getRandomUser(true);
            insertOperation(user);
            verifyResult(user);
            addTime();
            logger.debug("Test no: " + i + " executed");
        }
        logger.error("Executed with error rate: " + errors + " / " + iterations);
        logger.info(":ok");
    }

    private void verifyResult(Users user) throws ExecutionException, InterruptedException {
        boolean contains = true;
        if (!checkNodeContains(user))
            contains = false;

        if (!contains) {
            logger.error(":Wrong");
            errors++;
            isPositive = false;
        } else
            logger.info(":Ok");
    }

    private ResultSet selectOperation() throws SQLException {
        return cockroachContext
                .executeSelectQuery(GeneralQueries.selectFrom("*", TABLE_NAME));
    }

    private boolean resultSetContains(ResultSet resultSet, Users user) throws SQLException {
        boolean found = false;
        while (resultSet.next()) {
            if (resultSet.getString(1).equals(user.getLastname()))
                found = true;
        }
        if (!found)
            logger.error("Not found");
        return found;
    }

    public Callable<Boolean> getCallableResult(Users user) {
        return () -> {
            ResultSet resultSet = selectOperation();
            return resultSetContains(resultSet, user);
        };
    }

    @Override
    public Callable<Boolean> doCallableTask() {
        return null;
    }

}
