package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomString;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.Timer;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.TimestampHelper;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.ResultSet;
import java.sql.Timestamp;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class FirstConsistencyTestCockroach extends DbSingleTestCockroach {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private boolean isPositive = true;
    private CockroachContext cockroachContext;

    public FirstConsistencyTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        int i = Integer.parseInt(args);
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);
    }

    @Override
    public void performTest(String args) throws Exception {
        int iterations = Integer.parseInt(args);
        int performedIterations = 0;

        Users user = getRandomUser(true);
        String whatKind = Users.getColumnValueOrder();
        String whatValue = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String insertQuery = GeneralQueries.insertRow(TABLE_NAME, whatKind, whatValue);
        cockroachContext.executeInsertQuery(insertQuery);


        String selectQuery = GeneralQueries.selectFrom("count(*)", TABLE_NAME, "lastname = '" + user.getLastname() + "'");
        ResultSet selectResult = cockroachContext.executeSelectQuery(selectQuery);
        selectResult.next();
        if (selectResult.getInt(1) != 1) {
            logger.error("In database should be exactly one users with name " + user.getLastname() + " , " +
                    "but exists " + selectResult.getInt(1) + ". Validate this error with consistency.");
        }

        int tmp = 0;
        RandomString randomString = new RandomString();

        while (tmp < iterations) {
            String newFirstName = randomString.nextString();
            String updateQuery = GeneralQueries.updateRow(TABLE_NAME, "firstname = '"
                    + newFirstName + "'", "lastname = '" + user.getLastname() + "'");
            cockroachContext.executeInsertQuery(updateQuery);

            boolean found = false;
            int subIteration = 0;
            while ((!found) && (subIteration < 2)) {
                // select
                selectQuery = GeneralQueries.selectFrom("firstname", TABLE_NAME, "lastname = '" + user.getLastname() + "'");
                selectResult = cockroachContext.executeSelectQuery(selectQuery);
                selectResult.next();

                // check
                String newFirstNameBase = selectResult.getString(1);
                if (!newFirstNameBase.equalsIgnoreCase(newFirstName)) {
                    logger.error("In database should be user with name " + user.getLastname() + "  " +
                            "with first name: " + newFirstName + ", but his first name is: " + newFirstNameBase + ". Validate this error with consistency.");
                    isPositive = false;

                } else {
                    found = true;
                    logger.debug("Success in iteration no: " + tmp + ", and subiteration no: " + subIteration);
                    performedIterations++;
                    addTime();
                }
                subIteration++;
            }
            tmp++;
        }
        if (performedIterations == iterations)
            logger.info(":ok");
        else
            logger.error(":wrong");
    }

}
