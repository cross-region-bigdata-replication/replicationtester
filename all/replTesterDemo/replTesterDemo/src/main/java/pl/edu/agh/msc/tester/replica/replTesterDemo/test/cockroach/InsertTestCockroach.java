package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class InsertTestCockroach extends DbSingleTestCockroach {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CockroachContext cockroachContext;
    private long rowsAmount;
    private boolean isPositive = false;

    public InsertTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        ResultSet allRows = getAllRows("count(*)", TABLE_NAME);
        allRows.next();
        rowsAmount = allRows.getLong(1);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int insertCount = Integer.parseInt(args);

        for (int i = 0; i < insertCount; i++) {
            Users user = getRandomUser(true);
            insertOperation(user);
            logger.debug("Test no: " + i + " executed");
            addTime();
        }

        verifyResult(insertCount);
    }

    private void verifyResult(int insertCount) throws SQLException {
        ResultSet cockroachRs = getAllRows("count(*)", TABLE_NAME);
        cockroachRs.next();
        long insertedCount = cockroachRs.getLong(1);

        if (insertCount != (insertedCount - rowsAmount))
            logger.error(":Wrong");
        else {
            logger.info(":Ok");
            isPositive = true;
        }
    }

    private ResultSet getAllRows(String column, String table) throws SQLException {
        return cockroachContext.executeSelectQuery(GeneralQueries.selectFrom(column, table));
    }
}
