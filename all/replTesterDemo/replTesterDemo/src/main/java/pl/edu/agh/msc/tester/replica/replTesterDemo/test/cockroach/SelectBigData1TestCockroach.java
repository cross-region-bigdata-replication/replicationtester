package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 10.07.2018.
 * Package name is pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach
 * Time 01:41
 */
public class SelectBigData1TestCockroach extends DbSingleTestCockroach {

    private static final int MULTIPLY_ELEMENTS = 1;
    private final String TABLE_NAME = getTABLE_NAME();
    private static Logger logger;

    private CockroachContext cockroachContext;

    public SelectBigData1TestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(2000);

        logger.info(":prepared");
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < selectCount; i++) {
            try {
                selectCountWithCondition(1, i);
                logger.debug("Test no: " + i + " executed");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            addTime();
        }
        logger.info(":ok");
    }

    private void selectCountWithCondition(int selectCount, int arg) throws SQLException, InterruptedException {
        for (int i = 0; i < selectCount; i++) {
            ResultSet cockroachRs = cockroachContext
                    .executeSelectQuery(
                            GeneralQueries.selectFrom(
                                    "count(*)",
                                    TABLE_NAME,
                                    String.format("age=%d",arg))
                    );
            cockroachRs.next();
            logger.debug(String.format("age = %d -> count = %d;", arg, cockroachRs.getInt(1)));
        }
    }
}
