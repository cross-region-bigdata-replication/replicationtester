package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 18.05.2018.
 */
public class SelectTestCockroach extends DbSingleTestCockroach {

    private static final int MULTIPLY_ELEMENTS = 1;
    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME();

    private CockroachContext cockroachContext;

    public SelectTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return false;
    }

    @Override
    protected void performTest(String args) throws Exception {
        int selectCount = Integer.parseInt(args);

        for (int i = 0; i < selectCount; i++) {
            try {
                basicSelectTest(1);
                logger.debug("Test no: " + i + " executed");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            addTime();
        }
        logger.info(":ok");
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_NAME, columns));
        Thread.sleep(1000);

        int i = 100; //Integer.parseInt(args);
        Users user;

        for (int j = 0; j < i; j++) {
            user = getRandomUser(true);
            insertOperation(user);
        }
        logger.info(":prepared");
    }

    private void basicSelectTest(int selectCount) throws SQLException, InterruptedException {
        for (int i = 0; i < selectCount; i++) {
            ResultSet cockroachRs = cockroachContext
                    .executeSelectQuery(GeneralQueries.selectFrom("*", TABLE_NAME));
            while (cockroachRs.next()) {
                String name = cockroachRs.getString("lastname");
                int age = cockroachRs.getInt("age");
                logger.debug(String.format("%d. :: %s %d\n", i, name, age));
            }
        }
    }
}
