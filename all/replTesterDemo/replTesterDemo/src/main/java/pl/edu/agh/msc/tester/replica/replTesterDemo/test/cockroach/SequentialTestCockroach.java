package pl.edu.agh.msc.tester.replica.replTesterDemo.test.cockroach;

import org.slf4j.Logger;
import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.RandomUser;
import pl.edu.agh.msc.tester.replica.replTesterDemo.model.Users;
import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
import pl.edu.agh.msc.tester.replica.replTesterDemo.test.DbSingleTest;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Maciej Makowka on 19.05.2018.
 */
public class SequentialTestCockroach extends DbSingleTestCockroach {

    private static Logger logger;
    private final String TABLE_NAME = getTABLE_NAME(); //TODO
    private final String DB_NAME = LoadProperty.getString("cockroach.database_name");
    private final boolean FIRST_ASYNC = LoadProperty.getBoolean("test.seq.first_async");
    private final String TABLE_X = String.format("%s.%s", DB_NAME, "usersX");
    private final String TABLE_Y = String.format("%s.%s", DB_NAME, "usersY");

    private boolean isPositive = true;
    private CockroachContext cockroachContext;

    private int iterations = 0;
    private int errors = 0;

    public SequentialTestCockroach(String testName, int testRepetition, int testTimeout) throws Exception {
        super(testName, testRepetition, testTimeout);
        logger = getLogger();
        cockroachContext = getCockroachContext();
    }

    @Override
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    protected void prepareConnection() throws Exception {
        String columns = Users.getVarcharColumnDefinition();
        cockroachContext.executeDDLQuery(GeneralQueries.dropTable(TABLE_X));
        cockroachContext.executeDDLQuery(GeneralQueries.dropTable(TABLE_Y));
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_X, columns));
        cockroachContext.executeDDLQuery(GeneralQueries.createTable(TABLE_Y, columns));
        Thread.sleep(1000);
    }

    @Override
    protected void prepareTest(String args) throws Exception {
        int i = Integer.parseInt(args);
    }

    @Override
    protected void performTest(String args) throws Exception {
        int insertCount = 50;

        for (int i = 0; i < insertCount; i++) {
            addTime();
            Users user = getRandomUser(true);
            sequentialInsertTest(user);
            logger.debug(String.format("%d. :: Test performed", i));
            addTime();
            iterations++;

//            cockroachContext.executeDeleteQuery(GeneralQueries.deleteRow("usersX", "1=1"));
//            cockroachContext.executeDeleteQuery(GeneralQueries.deleteRow("usersY", "1=1"));
            cockroachContext.executeDeleteQuery(GeneralQueries.truncateTable(TABLE_X));
            cockroachContext.executeDeleteQuery(GeneralQueries.truncateTable(TABLE_Y));
        }

        logger.error("Executed with error rate: " + errors + " / " + iterations);
        logger.info(":ok");
    }

    private void sequentialInsertTest(Users user) {

        new Thread(() -> {
            try {
                selectData();
            } catch (SQLException e) {
                logger.error(String.valueOf(e));
            }
        }).start();

        new Thread(() -> {
            try {
                insertData(user);
            } catch (SQLException e) {
                logger.error(String.valueOf(e));
            }
        }).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void selectData() throws SQLException {

        String countTableXQuery = GeneralQueries.selectFrom("count(*)", TABLE_X);
        String countTableYQuery = GeneralQueries.selectFrom("count(*)", TABLE_Y);

        ResultSet resultX;
        ResultSet resultY = cockroachContext.executeSelectQuery(countTableYQuery);
        resultY.next();
        while (resultY.getInt(1) != 1) {
            resultY = cockroachContext.executeSelectQuery(countTableYQuery);
            resultY.next();
        }
        resultX = cockroachContext.executeSelectQuery(countTableXQuery);
        resultX.next();
        if (resultX.getInt(1) == 1) {
            logger.info(":Ok");
        } else {
            logger.error(":Wrong");
            isPositive = false;
            logger.error("result y= " + resultY.getInt(1) + "; result x= " + resultX.getInt(1));
            logger.error(resultY + " ; " + resultX);
            errors++;

            int i = 1;
            while (resultX.getInt(1) < 1 && i < 10) {
                resultX = cockroachContext.executeSelectQuery(countTableXQuery);
                resultX.next();
                i++;
            }
            if (i < 10) {
                logger.error(":Ok result after " + i + " next iterations.");
            } else {
                logger.error(":Wrong result ends test after" + i + " iterations.");
            }
        }
    }

    private void insertData(Users user) throws SQLException {
        String whatKindX = Users.getColumnValueOrder();
        String whatValueX = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String queryX = GeneralQueries.insertRow(TABLE_X, whatKindX, whatValueX);

        String whatKindY = Users.getColumnValueOrder();
        String whatValueY = "'" + user.getLastname() + "', " + user.getAge() + ", '" + user.getCity() + "', '"
                + user.getEmail() + "', '" + user.getFirstname() + "'";
        String queryY = GeneralQueries.insertRow(TABLE_Y, whatKindY, whatValueY);

        if (FIRST_ASYNC) {
            cockroachContext.executeInsertQueryAsync(queryX);
            logger.error("Async");
        }
        else {
            cockroachContext.executeInsertQuery(queryX);
            logger.error("NotAsync");
        }
        cockroachContext.executeInsertQueryAsync(queryY);
    }


}
