package pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.ConsoleColors;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.LoadProperty;

public class ResultPrinter {

    private static final Logger logger = LoggerFactory.getLogger(ResultPrinter.class);

    private static final int BRACKETS_AMOUNT = 2;
    private final String RESULT_LINE_PATTERN_NO_MSG = "\n%s%s[%s]";
    private final String RESULT_LINE_PATTERN_WITH_MSG = "\nNO IMPLEMENTED YET, TODO"; // TODO

    private static final int FIXED_WIDTH = 80;
    private static final String POSITIVE_MESSAGE = LoadProperty.getString("test.positive");
    private static final String NEGATIVE_MESSAGE = LoadProperty.getString("test.negative");
    private static final String POSITIVE_MESSAGE_COLOR = ConsoleColors.GREEN;
    private static final String NEGATIVE_MESSAGE_COLOR = ConsoleColors.RED;

    private final String testTitle;
    private final boolean isPositive;
    private String message;

    public ResultPrinter(String testTitle, boolean isPositive) {
        this.testTitle = testTitle;
        this.isPositive = isPositive;
    }

    public ResultPrinter(String testTitle, boolean isPositive, String message) {
        this.testTitle = testTitle;
        this.isPositive = isPositive;
        this.message = message;
    }

    public String printResultLine() {
        return resultLinePattern();
    }

    public String printResultLineWithMessage() {
        return "TODO"; // TODO
    }

    private String resultLinePattern() {
        int alreadyUsedSpace = calculateUsedSpace();
        String result;
        if (isPositive)
            result = wrapResultInColor(POSITIVE_MESSAGE, POSITIVE_MESSAGE_COLOR);
        else
            result = wrapResultInColor(NEGATIVE_MESSAGE, NEGATIVE_MESSAGE_COLOR);

        return String.format(RESULT_LINE_PATTERN_NO_MSG, testTitle, getDots(alreadyUsedSpace), result);
    }

    private int calculateUsedSpace() {
        int alreadyUsedSpace = 0;

        if (isPositive)
            alreadyUsedSpace += POSITIVE_MESSAGE.length();
        else
            alreadyUsedSpace += NEGATIVE_MESSAGE.length();

        alreadyUsedSpace += BRACKETS_AMOUNT;

        alreadyUsedSpace += testTitle.length();

        return alreadyUsedSpace;
    }

    private String wrapResultInColor(String result, String color) {
        return color + result + ConsoleColors.RESET;
    }

    private String getDots(int alreadyUsedSpace) {
        int howMany = FIXED_WIDTH - alreadyUsedSpace;
        if (howMany > 0)
            return Strings.repeat(".", howMany);
        else
            return "";
    }

}
