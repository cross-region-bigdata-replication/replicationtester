package pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYShapeRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.xy.XYDataset;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.msc.tester.replica.replTesterDemo.helper.StatisticsHelper;
import pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model.TimestampModel;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class TimestampChart extends ApplicationFrame {

    private static final int CHART_WIDTH = 768;
    private static final int CHART_HEIGHT = 768;
    private static final String CHART_X_AXIS_LABEL = "Measurement number";
    private static final String CHART_Y_AXIS_LABEL = "Response time [ms]";
    private static final String FILE_EXTENSION = ".svg";
    private static final Logger logger = LoggerFactory.getLogger(TimestampChart.class);

    private static String chartTitle;

    /**
     * Constructs a new application frame.
     *
     * @param title the frame title.
     */
    public TimestampChart(String title) {
        super(title);
    }

    public static void run(String title, List<TimestampModel> timestampModels) throws IOException {
        chartTitle = title;
        TimestampChart timestampChart = new TimestampChart(title, timestampModels);
        timestampChart.pack();
        timestampChart.setVisible(true);
    }


    private TimestampChart(String title, List<TimestampModel> timestampModels) throws IOException {
        super(title);

        StatisticsHelper statisticsHelper = new StatisticsHelper(title);
        XYDataset xyDataset = statisticsHelper.createDataSet(timestampModels);
        JFreeChart xyLineChart = ChartFactory.createXYLineChart(
                title,
                CHART_X_AXIS_LABEL,
                CHART_Y_AXIS_LABEL,
                xyDataset,
                PlotOrientation.VERTICAL,
                true, false, false
        );

        ChartPanel chartPanel = new ChartPanel(xyLineChart);
        chartPanel.setPreferredSize(new Dimension(CHART_WIDTH, CHART_HEIGHT));
        final XYPlot xyPlot = xyLineChart.getXYPlot();
        XYShapeRenderer xyShapeRenderer = new XYShapeRenderer();
        xyShapeRenderer.setSeriesPaint(0, Color.BLUE);
        xyShapeRenderer.setSeriesShape(0, new Rectangle2D.Float());
        xyShapeRenderer.setSeriesShape(0, new Ellipse2D.Float(7.5f, 7.5f, 7.5f, 7.5f));
        xyShapeRenderer.setSeriesPaint(1, Color.RED);
        xyShapeRenderer.setSeriesPaint(2, Color.GRAY);
        xyPlot.setRenderer(xyShapeRenderer);
        xyPlot.setBackgroundPaint(Color.WHITE);
        adjustAxis((NumberAxis) xyPlot.getDomainAxis(), 0, xyDataset.getItemCount(0), true);
        adjustAxis((NumberAxis) xyPlot.getRangeAxis(), 45, 130, true);
        setContentPane(chartPanel);

        SVGGraphics2D g2 = new SVGGraphics2D(CHART_WIDTH, CHART_HEIGHT);
        Rectangle rectangle = new Rectangle(0, 0, CHART_WIDTH, CHART_HEIGHT);
        g2.setBackground(Color.WHITE);
        xyLineChart.draw(g2, rectangle);
        File file = new File(this.getTitle() + FILE_EXTENSION);
        SVGUtils.writeToSVG(file, g2.getSVGElement());
    }

    private void adjustAxis(NumberAxis axis, int lower, int upper, boolean verticalTickLabels) {
        axis.setRange(lower, upper);
        axis.setVerticalTickLabels(verticalTickLabels);
        axis.setAutoTickUnitSelection(true);
    }
}
