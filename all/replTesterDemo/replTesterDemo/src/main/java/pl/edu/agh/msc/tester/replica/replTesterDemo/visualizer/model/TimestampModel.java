package pl.edu.agh.msc.tester.replica.replTesterDemo.visualizer.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TimestampModel {

    private List<Timestamp> timestamp = new ArrayList<>();
    private String label;

    public TimestampModel(String label) {
        this.label = label;
    }

    public List<Timestamp> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(List<Timestamp> timestamp) {
        this.timestamp = timestamp;
    }

    public String getLabel() {
        return label;
    }
}
