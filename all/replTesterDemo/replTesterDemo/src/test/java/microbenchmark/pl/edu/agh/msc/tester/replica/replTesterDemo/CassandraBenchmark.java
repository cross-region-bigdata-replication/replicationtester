//package microbenchmark.pl.edu.agh.msc.tester.replica.replTesterDemo;
//
//
//import com.datastax.driver.core.ResultSet;
//import com.datastax.driver.core.Session;
//import com.google.caliper.AfterExperiment;
//import com.google.caliper.BeforeExperiment;
//import com.google.caliper.Benchmark;
//import com.google.caliper.Param;
//import com.google.caliper.api.VmOptions;
//import com.google.caliper.runner.CaliperMain;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CassandraContext;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.scenario.cassandra.CassandraTestScenario;
//
//import java.io.IOException;
//import java.sql.SQLException;
//
//@VmOptions("-XX:-TieredCompilation")
//public class CassandraBenchmark {
//    public static void main(String[] args) {
//        CaliperMain.main(CassandraBenchmark.class, args);
//    }
//
//    @Param({"1", "100"})
//    private int selectAllCount;
//
//    private CassandraContext cassandraContext;
//    private Session session;
//
//    @BeforeExperiment
//    void setUp() throws InterruptedException {
//        cassandraContext = new CassandraContext();
//        session = cassandraContext.getBasicSession();
////        Thread.sleep(500);
//    }
//
//    @AfterExperiment
//    void setDown() throws InterruptedException {
////        Thread.sleep(500);
//        cassandraContext.cleanDefaultKeyspace();
//        cassandraContext.closeSession(session);
//    }
////
////    @Benchmark
////    void timeStringBuilder(int reps) {
////        StringBuilder sb = new StringBuilder();
////        for (int i = 0; i < reps; i++) {
////            sb.setLength(0);
////            sb.append("hello world");
////        }
////    }
//
//    @Benchmark
//    void insertExampleUsers(int reps) throws SQLException, ClassNotFoundException, IOException {
//        for (int i = 0; i < reps; i++) {
//            for (int j = 1; j <= selectAllCount; j=j*10) {
//                System.out.println("@TEST CASSANDRA");
//                System.out.println("J= " + j);
//                CassandraTestScenario cassandraTestScenario = new CassandraTestScenario();
//                cassandraTestScenario.performBasicInsertTest(j);
//            }
//        }
//    }
//
//    @Benchmark
//    ResultSet selectAllUsers(int reps) {
//        String query = GeneralQueries.selectFrom("*", "users");
//        ResultSet resultSet = null;
//        for (int i = 0; i < reps; i++) {
//            for (int j = 1; j <= selectAllCount; j=j*10) {
//                System.out.println("J= " + j);
//                resultSet = cassandraContext.executeIdempotentQuery(session, query);
//            }
//        }
//        return resultSet;
//    }
//
//}
