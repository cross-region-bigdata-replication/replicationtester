//package microbenchmark.pl.edu.agh.msc.tester.replica.replTesterDemo;
//
//import com.google.caliper.AfterExperiment;
//import com.google.caliper.BeforeExperiment;
//import com.google.caliper.Benchmark;
//import com.google.caliper.Param;
//import com.google.caliper.api.VmOptions;
//import com.google.caliper.runner.CaliperMain;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.context.CockroachContext;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.query.GeneralQueries;
//import pl.edu.agh.msc.tester.replica.replTesterDemo.scenario.cockroach.CockroachTestScenario;
//
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//@VmOptions("-XX:-TieredCompilation")
//public class CockroachBenchmark {
//
//    public static void main(String[] args) {
//        CaliperMain.main(CockroachBenchmark.class, args);
//    }
//
//    @Param({"1", "100"})
//    private int selectAllCount;
//
//    private CockroachContext cockroachContext;
//    private Connection connection;
//
//    @BeforeExperiment
//    void setUp() throws SQLException, ClassNotFoundException, InterruptedException {
//        cockroachContext = new CockroachContext();
//        connection = cockroachContext.getBasicConnection();
////        Thread.sleep(500);
//    }
//
//    @AfterExperiment
//    void setDown() throws InterruptedException, SQLException {
////        Thread.sleep(500);
//        cockroachContext.closeConnection(connection);
//    }
////
////    @Benchmark
////    void timeStringBuilder(int reps) {
////        StringBuilder sb = new StringBuilder();
////        for (int i = 0; i < reps; i++) {
////            sb.setLength(0);
////            sb.append("hello world");
////        }
////    }
//
//    @Benchmark
//    void insertExampleUsers(int reps) throws SQLException, ClassNotFoundException, IOException {
//        for (int i = 0; i < reps; i++) {
//            for (int j = 1; j <= selectAllCount; j=j*10) {
//                System.out.println("@TEST COCKROACH");
//                System.out.println("J= " + j);
//                CockroachTestScenario cockroachTestScenario = new CockroachTestScenario();
//                cockroachTestScenario.performBasicInsertTest(j);
//            }
//        }
//    }
//
//    @Benchmark
//    ResultSet selectAllUsers(int reps) throws SQLException {
//        String query = GeneralQueries.selectFrom("*", "users");
//        ResultSet resultSet = null;
//        for (int i = 0; i < reps; i++) {
//            for (int j = 1; j <= selectAllCount; j=j*10) {
//                resultSet = cockroachContext.executeSelectQuery(connection, query);
//            }
//        }
//        return resultSet;
//    }
//
//}
