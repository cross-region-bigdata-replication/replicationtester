package pl.mgr.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.AddressTranslater;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static final String KEYSPACE_CREATION_QUERY = "CREATE KEYSPACE IF NOT EXISTS testKeySpace " + "WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' };";
    public static final String KEYSPACE_ACTIVATE_QUERY = "USE testKeySpace;";
    public static final String TABLE_CREATION_QUERY = "CREATE TABLE IF NOT EXISTS testKeySpace.users (lastname text PRIMARY KEY, age int, city text, email text, firstname text);";
    public static final String DATA_TABLE_NAME = "book";

    private static Cluster cluster;
    private static Session session;

    public static void main(String[] args) {
        List<InetSocketAddress> addresses = new ArrayList<>();
        addresses.add(new InetSocketAddress("149.156.11.4", 10376));
        addresses.add(new InetSocketAddress("149.156.11.4", 10367));
        addresses.add(new InetSocketAddress("149.156.11.4", 10368));
        AddressTranslater addressTranslater = new AddressTranslater() {
            @Override
            public InetSocketAddress translate(InetSocketAddress inetSocketAddress) {
                String ADDRESS_OUT = "149.156.11.4";
                String ADDRESS_1_IN = "172.18.0.4";
                int ADDRESS_1_OUT_PORT = 10367;
                String ADDRESS_2_IN = "172.18.0.5";
                int ADDRESS_2_OUT_PORT = 10368;

                if (inetSocketAddress.getHostName().compareTo(ADDRESS_1_IN) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, ADDRESS_1_OUT_PORT);
                if (inetSocketAddress.getHostName().compareTo(ADDRESS_2_IN) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, ADDRESS_2_OUT_PORT);
                System.out.println("BAD ADDRESS, NO MATCH :: " + inetSocketAddress.getHostName());
                return null;
            }
        };
//        cluster = Cluster.builder().addContactPointsWithPorts(addresses).withAddressTranslater(addressTranslater).build();
        cluster = Cluster.builder().addContactPoint("149.156.11.4").withPort(10376).withAddressTranslater(addressTranslater).build();
        session = cluster.connect();
//        session = cluster.connect("demo");
        session.execute(KEYSPACE_CREATION_QUERY);
        session.execute(KEYSPACE_ACTIVATE_QUERY);
        session.execute(TABLE_CREATION_QUERY);
        session.execute("INSERT INTO users (lastname, age, city, email, firstname) VALUES ('Jones', 25, 'Austin', 'bob@ex.com', 'Bob')");

        ResultSet resultSet = session.execute("SELECT * FROM users WHERE lastname='Jones'");
        for (Row row : resultSet) {
            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
        }

        // Update the same user with a new age
        session.execute("update users set age = 36 where lastname = 'Jones'");
        // Select and show the change
        resultSet = session.execute("select * from users where lastname='Jones'");
        for (Row row : resultSet) {
            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));

        }

        // Delete the user from the users table
        session.execute("DELETE FROM users WHERE lastname = 'Jones'");
        // Show that the user is gone
        resultSet = session.execute("SELECT * FROM users");
        for (Row row : resultSet) {
            System.out.format("%s %d %s %s %s\n", row.getString("lastname"), row.getInt("age"), row.getString("city"), row.getString("email"), row.getString("firstname"));
        }

        // Clean up the connection by closing it
        cluster.close();
    }
}
