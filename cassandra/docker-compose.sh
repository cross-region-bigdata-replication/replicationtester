version: '2'

services:
  db-one:
    image: cassandra:2
    command: bash -c 'if [ -z "$$(ls -A /var/lib/cassandra/)" ] ; then sleep 0; fi && /docker-entrypoint.sh cassandra -f'
    hostname: db-one
    expose:
      - 7000
      - 7001
      - 7199
      - 9042
      - 9160
    networks:
      - mgrnet
    volumes:
      - ./data/db-one:/cassandra/cassandra-data
    environment:
      - CASSANDRA_CLUSTER_NAME=dev_cluster
      - CASSANDRA_SEEDS=db-one,db-two,db-three
    ulimits:
      memlock: -1
      nproc: 32768
      nofile: 100000
    ports:
      - "9043:9042"
    
  db-two:
    image: cassandra:2
    command: bash -c 'if [ -z "$$(ls -A /var/lib/cassandra/)" ] ; then sleep 60; fi && /docker-entrypoint.sh cassandra -f'
    networks:
      - mgrnet
    volumes:
      - ./data/db-two:/cassandra/cassandra-data
    expose:
      - 7000
      - 7001
      - 7199
      - 9042
      - 9160
    environment:
      - CASSANDRA_CLUSTER_NAME=dev_cluster
      - CASSANDRA_SEEDS=db-one,db-two,db-three
    depends_on:
      - db-one
    ulimits:
      memlock: -1
      nproc: 32768
      nofile: 100000
    ports:
      - "9044:9042"

  db-three:
    image: cassandra:2
    command: bash -c 'if [ -z "$$(ls -A /var/lib/cassandra/)" ] ; then sleep 120; fi && /docker-entrypoint.sh cassandra -f'
    networks:
      - mgrnet
    volumes:
      - ./data/db-three:/cassandra/cassandra-data
    expose:
      - 7000
      - 7001
      - 7199
      - 9042
      - 9160
    environment:
      - CASSANDRA_CLUSTER_NAME=dev_cluster
      - CASSANDRA_SEEDS=db-one,db-two,db-three
    depends_on:
      - db-one
    ulimits:
      memlock: -1
      nproc: 32768
      nofile: 100000   
    ports:
      - "9045:9042"

  portainer:
    image: portainer/portainer
    command: --templates http://templates/templates.json
    networks:
      - mgrnet
    volumes:
      - ./portainer-data:/data
      - /var/run/docker.sock:/var/run/docker.sock
    ports:
      - "9000:9000"    

networks:
  mgrnet:
