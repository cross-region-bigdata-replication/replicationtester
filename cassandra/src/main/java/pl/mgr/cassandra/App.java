package pl.mgr.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class App {
    public static final String KEYSPACE_CREATION_QUERY = "CREATE KEYSPACE IF NOT EXISTS testKeySpace " + "WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '3' };";
    public static final String KEYSPACE_ACTIVATE_QUERY = "USE testKeySpace;";
    public static final String DATA_TABLE_NAME = "book";

    private Cluster cluster;
    private Session session;

    public static void main(String[] args) {
        
    }
}
