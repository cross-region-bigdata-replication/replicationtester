package pl.edu.agh.msc.test.replication;

import com.google.common.collect.ImmutableSet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import pl.edu.agh.msc.test.replication.helper.TimestampHelper;
import pl.edu.agh.msc.test.replication.model.Users;
import pl.edu.agh.msc.test.replication.repository.cassandra.UsersRepository;

import java.sql.Timestamp;

@SpringBootApplication
@ComponentScan("pl.edu.agh.msc.test.replication.repository, pl.edu.agh.msc.test.replication.dao, pl.edu.agh.msc.test.replication.config")
public class WebLoadtesterApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(WebLoadtesterApplication.class, args);
        UsersRepository usersRepository = context.getBean(UsersRepository.class);
//
        System.out.println("HELLO 0");

        test(usersRepository);

        System.out.println("HELLO 2");
    }

    private static void test(UsersRepository usersRepository) {
        Users newUser = new Users("mackston", 29, "krakow", "bob@johnson.com", "bob");
        System.out.println("HELLO 1");
        usersRepository.insert(newUser);
        //usersRepository.save(newUser);
    }

    private static Timestamp getCurrentTimestamp() {
        return TimestampHelper.getCurrentTimestamp();
    }

}
