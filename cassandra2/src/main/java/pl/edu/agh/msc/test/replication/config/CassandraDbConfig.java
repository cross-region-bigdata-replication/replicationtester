package pl.edu.agh.msc.test.replication.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.policies.AddressTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import java.net.InetSocketAddress;

@Configuration
@EnableCassandraRepositories(basePackages = "pl.edu.agh.msc.test.replication.repository.cassandra")
public class CassandraDbConfig extends AbstractCassandraConfiguration {

    private static final String DATA_CENTER_HOST = "149.156.11.4";
    private static final String KEYSPACE_NAME = "testKeySpace";
    public static final String DATA_TABLE_NAME = "users";

    private static final int SEED_PORT = 10376;
    private static final int DB1_PORT = 10367;
    private static final int DB2_PORT = 10368;

    @Override
    protected String getKeyspaceName() {
        return "testKeySpace";
    }

    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
        cluster.setContactPoints(DATA_CENTER_HOST);
        cluster.setPort(SEED_PORT);
        cluster.setAddressTranslator(initAddressTranslater());
        return cluster;
    }

    private AddressTranslator initAddressTranslater() {

        AddressTranslator addressTranslator = new AddressTranslator() {
            @Override
            public void init(Cluster cluster) {

            }

            @Override
            public InetSocketAddress translate(InetSocketAddress inetSocketAddress) {
                String ADDRESS_OUT = DATA_CENTER_HOST;
                String ADDRESS_1_IN = "172.18.0.4";
                int ADDRESS_1_OUT_PORT = DB1_PORT;
                String ADDRESS_2_IN = "172.18.0.5";
                int ADDRESS_2_OUT_PORT = DB2_PORT;

                if (inetSocketAddress.getHostName().compareTo(ADDRESS_1_IN) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, ADDRESS_1_OUT_PORT);
                if (inetSocketAddress.getHostName().compareTo(ADDRESS_2_IN) == 0)
                    return new InetSocketAddress(ADDRESS_OUT, ADDRESS_2_OUT_PORT);
                System.out.println("BAD ADDRESS, NO MATCH :: " + inetSocketAddress.getHostName());
                return null;
            }

            @Override
            public void close() {

            }
        };
        return addressTranslator;
    }

    @Override
    public CassandraMappingContext cassandraMapping() throws ClassNotFoundException {
        return new BasicCassandraMappingContext();
    }


}
