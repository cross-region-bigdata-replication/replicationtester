package pl.edu.agh.msc.test.replication.helper;

public enum DbNode {
    INSERTION, DIFFERENT;
}
