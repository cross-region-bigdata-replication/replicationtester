package pl.edu.agh.msc.test.replication.model;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table
public class Users {

//    @PrimaryKeyColumn(
//            name="id",
//            type = PrimaryKeyType.PARTITIONED
//    )
//    private UUID id;

    @PrimaryKeyColumn(
            name="lastname",
            type = PrimaryKeyType.PARTITIONED
    )
    private String lastname;

    @Column
    private int age;

    @Column
    private String city;

    @Column
    private String email;

    @Column
    private String firstname;

    public Users(String lastname, int age, String city, String email, String firstname) {
        this.lastname = lastname;
        this.age = age;
        this.city = city;
        this.email = email;
        this.firstname = firstname;
    }
}
