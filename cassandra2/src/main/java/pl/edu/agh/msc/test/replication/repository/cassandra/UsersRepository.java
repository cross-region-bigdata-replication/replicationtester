package pl.edu.agh.msc.test.replication.repository.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msc.test.replication.model.Users;

@Repository
public interface UsersRepository extends CassandraRepository<Users, Users>{
    //TODO: sth wrong with above CassandraRepository brackets (in tutorial only one argument)
}
