package pl.edu.agh.msc.test.replication.visualizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYShapeRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import pl.edu.agh.msc.test.replication.visualizer.model.TimestampDataModel;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class TimestampChartAWT extends ApplicationFrame {

    private static final int CHART_WIDTH = 1920;
    private static final int CHART_HEIGHT = 1080;
    private static final String CHART_X_AXIS_LABEL = "Measurement number";
    private static final String CHART_Y_AXIS_LABEL = "Response time [ms]";
    private static final String SVG_EXTENSION = ".svg";

    private int measureNo = 0;
    private long lastTimestamp = 0;

    public TimestampChartAWT(String title) {
        super(title);
    }

    public static void run(String title, List<TimestampDataModel> timestampDataModels) throws IOException {
        TimestampChartAWT chart = new TimestampChartAWT(title, timestampDataModels);
        chart.pack();
        chart.setVisible(true);
    }

    private TimestampChartAWT(String title, List<TimestampDataModel> timestampDataModels) throws IOException {
        super(title);

        XYDataset xyDataset = createDataSet(timestampDataModels);
        JFreeChart xyLineChart = ChartFactory.createXYLineChart(
                title,
                CHART_X_AXIS_LABEL,
                CHART_Y_AXIS_LABEL,
                xyDataset,
                PlotOrientation.VERTICAL,
                true, false, false
        );

        ChartPanel chartPanel = new ChartPanel(xyLineChart);
        chartPanel.setPreferredSize(new Dimension(CHART_WIDTH, CHART_HEIGHT));
        final XYPlot xyPlot = xyLineChart.getXYPlot();
        XYShapeRenderer xyShapeRenderer = new XYShapeRenderer();
        xyShapeRenderer.setSeriesPaint(0, Color.GRAY);
        xyShapeRenderer.setSeriesPaint(1, Color.RED);
        xyShapeRenderer.setSeriesPaint(2, Color.BLUE);
        xyPlot.setRenderer(xyShapeRenderer);
        xyPlot.setBackgroundPaint(Color.WHITE);
        setContentPane(chartPanel);

        SVGGraphics2D g2 = new SVGGraphics2D(CHART_WIDTH, CHART_HEIGHT);
        Rectangle rectangle = new Rectangle(0, 0, CHART_WIDTH, CHART_HEIGHT);
        g2.setBackground(Color.WHITE);
        xyLineChart.draw(g2, rectangle);
        File file = new File(this.getTitle() + SVG_EXTENSION);
        SVGUtils.writeToSVG(file, g2.getSVGElement());
    }

    private XYDataset createDataSet(List<TimestampDataModel> timestampDataModels) {

        final XYSeriesCollection dataSet = new XYSeriesCollection();

        timestampDataModels.forEach(tdm -> {
            setLastTimestamp(tdm.getTimestamp().get(0).getTime());
            XYSeries series = new XYSeries(tdm.getLabel());
            tdm.getTimestamp().forEach(tms -> {
                series.add(updateMeasureNo(), tms.getTime() - getLastTimestamp());
                setLastTimestamp(tms.getTime());
            });
            dataSet.addSeries(series);
        });
        return dataSet;
    }

    private int updateMeasureNo() {
        measureNo++;
        return measureNo;
    }

    private long getLastTimestamp() {
        return lastTimestamp;
    }

    private void setLastTimestamp(long timestamp) {
        lastTimestamp = timestamp;
    }

}
