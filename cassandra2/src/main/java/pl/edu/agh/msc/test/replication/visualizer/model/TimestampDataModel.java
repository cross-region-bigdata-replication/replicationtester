package pl.edu.agh.msc.test.replication.visualizer.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TimestampDataModel {

    private List<Timestamp> timestamp = new ArrayList<>();
    private String label;

    public TimestampDataModel(String label) {
        this.label = label;
    }

    public List<Timestamp> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(List<Timestamp> timestamp) {
        this.timestamp = timestamp;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
