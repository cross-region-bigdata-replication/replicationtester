package pl.edu.agh.msc.test.replication.dao.node.insert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msc.test.replication.model.Customer;
import pl.edu.agh.msc.test.replication.model.IDbModel;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class InsertionInsertDao extends JdbcDaoSupport implements IInsertionInsertDao {

    @Qualifier("dsNode1")
    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public void initTable() {
        String sql = "" +
                "CREATE TABLE customer (\n" +
                "    CUST_ID Bigserial PRIMARY KEY NOT NULL,\n" +
                "    T_S TIMESTAMP NOT NULL,\n" +
                "    NAME VARCHAR (100) NOT NULL,\n" +
                "    AGE SMALLINT  NOT NULL\n" +
                ");";
        getJdbcTemplate().update(sql);
    }
    @Override
    public void insert(IDbModel customer) {
        String sql = "INSERT INTO customer " +
                "(CUST_ID, T_S, NAME, AGE) VALUES (?, ?, ?, ?)";
        Customer cus = (Customer) customer;
        getJdbcTemplate().update(sql, cus.getId(), cus.getT_s(), cus.getName(), cus.getAge());
    }

    @Override
    public void insertCustomerList(List<IDbModel> customerList) {
        String sql = "INSERT INTO customer " + "(CUST_ID, T_S, NAME, AGE) VALUES (?, ?, ?, ?)";
        getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Customer customer = (Customer) customerList.get(i);
                ps.setLong(1, customer.getId());
                ps.setTimestamp(2, customer.getT_s());
                ps.setString(3, customer.getName());
                ps.setInt(4, customer.getAge());
            }

            public int getBatchSize() {
                return customerList.size();
            }
        });
    }
}
