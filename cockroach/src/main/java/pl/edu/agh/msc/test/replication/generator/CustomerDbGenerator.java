package pl.edu.agh.msc.test.replication.generator;

import pl.edu.agh.msc.test.replication.helper.RandomString;
import pl.edu.agh.msc.test.replication.helper.TimestampHelper;
import pl.edu.agh.msc.test.replication.model.Customer;
import pl.edu.agh.msc.test.replication.service.IInsertionService;
import pl.edu.agh.msc.test.replication.service.ISelectionService;

import java.sql.Timestamp;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class CustomerDbGenerator implements IDbDataGenerator {

    private static final int DB_INDEX_RANGE = 90_000_000;
    private static final int CUSTOMER_AGE_MAX = 99;
    private static final int CUSTOMER_NAME_SIZE = 10;

    @Override
    public boolean cleanTable(IInsertionService iService) {
        IInsertionService customerService = (IInsertionService) iService;
        return false;
    }

    @Override
    public Customer generateCustomer(IInsertionService iService) {
        IInsertionService customerService = (IInsertionService) iService;

        return generateCustomer();
    }

    private Timestamp getCurrentTimestamp() {
        return TimestampHelper.getCurrentTimestamp();
    }

    private Customer generateCustomer() {
        Customer customer = new Customer();
        Random rand = new Random();

        RandomString randomString = new RandomString(CUSTOMER_NAME_SIZE, ThreadLocalRandom.current());
        Long customerId = (long) rand.nextInt(DB_INDEX_RANGE) + 1;
        String name = randomString.nextString();
        int age = rand.nextInt(CUSTOMER_AGE_MAX) + 1;
        customer.setId(customerId);
        customer.setName(name);
        customer.setAge(age);
        customer.setT_s(getCurrentTimestamp());

        return customer;
    }

}
