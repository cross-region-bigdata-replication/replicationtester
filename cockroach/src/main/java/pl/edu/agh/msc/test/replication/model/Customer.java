package pl.edu.agh.msc.test.replication.model;

import java.sql.Timestamp;

public class Customer implements IDbModel {
    private static final long serialVersionUID = 1L;
    long custId;
    Timestamp t_s;
    String name;
    int age;

    public Customer() {
    }

    public Customer(long custId, String name, int age) {
        this.custId = custId;
        this.name = name;
        this.age = age;
    }

    public Customer(long custId, Timestamp t_s, String name, int age) {
        this.custId = custId;
        this.t_s = t_s;
        this.name = name;
        this.age = age;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return custId;
    }

    public void setId(long custId) {
        this.custId = custId;
    }

    public Timestamp getT_s() {
        return t_s;
    }

    public void setT_s(Timestamp t_s) {
        this.t_s = t_s;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Customer {" +
                "custId=" + custId +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", timestamp= " + t_s +
                '}';
    }


}
