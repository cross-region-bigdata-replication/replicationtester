package pl.edu.agh.msc.test.replication.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Orders implements IDbModel {

    private static final long serialVersionUID = 1L;
    long OrderId;
    Timestamp t_s;
    String title;
    String description;
    BigDecimal value;
    long FkCustId;


    public Orders() {
    }

    public Orders(long orderId, Timestamp t_s, String title, String description, BigDecimal value, long fkCustId) {
        OrderId = orderId;
        this.t_s = t_s;
        this.title = title;
        this.description = description;
        this.value = value;
        FkCustId = fkCustId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return OrderId;
    }

    public void setId(long orderId) {
        OrderId = orderId;
    }

    public Timestamp getT_s() {
        return t_s;
    }

    public void setT_s(Timestamp t_s) {
        this.t_s = t_s;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public long getFkCustId() {
        return FkCustId;
    }

    public void setFkCustId(long fkCustId) {
        FkCustId = fkCustId;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "OrderId=" + OrderId +
                ", t_s=" + t_s +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", value=" + value +
                ", FkCustId=" + FkCustId +
                '}';
    }
}
