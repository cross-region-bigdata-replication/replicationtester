package pl.edu.agh.msc.test.replication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.msc.test.replication.dao.node.insert.IInsertionInsertDao;
import pl.edu.agh.msc.test.replication.model.IDbModel;

import java.util.List;

@Service
public class InsertionService implements IInsertionService {

    @Autowired
    IInsertionInsertDao customerDao;

    @Override
    public void initTable() {
        customerDao.initTable();
    }

    @Override
    public void insert(IDbModel cus) {
        customerDao.insert(cus);
    }

    @Override
    public void insertRowList(List<IDbModel> customers) {
        customerDao.insertCustomerList(customers);
    }

}
