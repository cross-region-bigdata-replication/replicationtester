package pl.edu.agh.msc.test.replication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import pl.edu.agh.msc.test.replication.clients.InsertionClient;
import pl.edu.agh.msc.test.replication.clients.SelectionClient;
import pl.edu.agh.msc.test.replication.helper.TimestampHelper;
import pl.edu.agh.msc.test.replication.service.*;

import java.sql.Timestamp;

@SpringBootApplication
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ComponentScan("pl.edu.agh.msc.test.replication.service, pl.edu.agh.msc.test.replication.dao, pl.edu.agh.msc.test.replication.config")
public class WebLoadtesterApplication {

    private static final String SERVICE_1  = "";
    private static final String SERVICE_2  = "";
    private static final String SERVICE_3  = "";

    @Autowired
    IInsertionService insertionService;

    @Autowired
    ISelectionService selectionService;

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(WebLoadtesterApplication.class, args);
        IInsertionService insertionService = context.getBean(IInsertionService.class);
//        ISelectionService selectionService = context.getBean(ISelectionService.class);
        ISelectionService selectionService1 = context.getBean(ISelectionService.class);
        IDifferentService selectionService2 = context.getBean(IDifferentService.class);
        IDifferentService selectionService3 = context.getBean(IDifferentService.class);

        InsertionClient insertionClient = new InsertionClient(insertionService);
        SelectionClient selectionClient1 = new SelectionClient(selectionService1);
        SelectionClient selectionClient2 = new SelectionClient(selectionService2);
        SelectionClient selectionClient3 = new SelectionClient(selectionService3);

        Thread t1 = new Thread(insertionClient);
        Thread t2 = new Thread(selectionClient1);
        Thread t3 = new Thread(selectionClient2);
        Thread t4 = new Thread(selectionClient3);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.start();
        t2.start();
        t3.start();
//        t4.start();


    }

    private static Timestamp getCurrentTimestamp() {
        return TimestampHelper.getCurrentTimestamp();
    }

}
