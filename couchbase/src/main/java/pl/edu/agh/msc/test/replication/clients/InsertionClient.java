package pl.edu.agh.msc.test.replication.clients;

import pl.edu.agh.msc.test.replication.generator.CustomerDbGenerator;
import pl.edu.agh.msc.test.replication.helper.TimestampHelper;
import pl.edu.agh.msc.test.replication.model.Customer;
import pl.edu.agh.msc.test.replication.service.IInsertionService;
import pl.edu.agh.msc.test.replication.visualizer.TimestampChartAWT;
import pl.edu.agh.msc.test.replication.visualizer.model.TimestampDataModel;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class InsertionClient implements Runnable {

    private static final String SERIES_NAME_1 = "Period 1";
    private static final String SERIES_NAME_2 = "Period 2";
    private static final String SERIES_NAME_3 = "Period 3";

    private IInsertionService insertionService;
    private List<TimestampDataModel> timestampDataModelList = new ArrayList<>();

    public InsertionClient(IInsertionService insertionService) {
        this.insertionService = insertionService;
    }

    @Override
    public void run() {
        int i = 0;

        TimestampDataModel tdm1 = new TimestampDataModel(SERIES_NAME_1);
        TimestampDataModel tdm2 = new TimestampDataModel(SERIES_NAME_2);
        TimestampDataModel tdm3 = new TimestampDataModel(SERIES_NAME_3);

        ArrayList<Timestamp> tms1 = new ArrayList<>();
        ArrayList<Timestamp> tms2 = new ArrayList<>();
        ArrayList<Timestamp> tms3 = new ArrayList<>();

        while (i < 100) {
            try {
                waitUntilZeroSecond();
                tms1.add(addNewCustomer());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        System.out.println("Period 1 ended. Press enter to continue.");
        try {
            System.in.read();
            System.out.println("Enter received");
        } catch (IOException e) {
            System.out.println("Error occurred");
            e.printStackTrace();
        }
        while (i < 200) {
            try {
                waitUntilZeroSecond();
                tms2.add(addNewCustomer());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        System.out.println("Period 2 ended. Press enter to continue.");
        try {
            System.in.read();
            System.out.println("Enter received");
        } catch (IOException e) {
            System.out.println("Error occurred");
            e.printStackTrace();
        }
        while (i < 300) {
            try {
                waitUntilZeroSecond();
                tms3.add(addNewCustomer());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        System.out.println("Period 3 ended.");
        tdm1.setTimestamp(tms1);
        tdm2.setTimestamp(tms2);
        tdm3.setTimestamp(tms3);

        timestampDataModelList.add(tdm1);
        timestampDataModelList.add(tdm2);
        timestampDataModelList.add(tdm3);
        try {
            TimestampChartAWT.run("Response time for insertion", timestampDataModelList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Timestamp addNewCustomer() {
        Timestamp timestamp;
        CustomerDbGenerator customerDbGenerator = new CustomerDbGenerator();
        Customer customer = customerDbGenerator.generateCustomer(insertionService);
        insertionService.insert(customer);
        timestamp = TimestampHelper.getCurrentTimestamp();
        return timestamp;
//        System.out.println("LOG: add new customer :: " + TimestampHelper.getCurrentTimestamp());
//        System.out.println(timestamp.getTime());
    }

    private void waitUntilZeroSecond() throws InterruptedException {
//        long timeToZero = 60 - TimestampHelper.getCurrentTimestamp().getSeconds();
//        Thread.sleep(timeToZero * 1000);
//        System.out.println("LOG: zero second");
        Thread.sleep(0);
    }

}
