package pl.edu.agh.msc.test.replication.clients;

import pl.edu.agh.msc.test.replication.helper.DbNode;
import pl.edu.agh.msc.test.replication.helper.TimestampHelper;
import pl.edu.agh.msc.test.replication.service.IDifferentService;
import pl.edu.agh.msc.test.replication.service.ISelectionService;

import java.sql.Timestamp;

public class SelectionClient implements Runnable {

    private DbNode dbNode;
    private IDifferentService differentService;
    private ISelectionService selectionService;
    private int customerAmount;

    private int iteration = 0;
    private int lastUpdate = 0;
    private Timestamp previousTimestamp;

    public SelectionClient(ISelectionService selectionService) {
        this.selectionService = selectionService;
        customerAmount = selectionService.getTotalRowNumber();
        dbNode = DbNode.INSERTION;
    }

    public SelectionClient(IDifferentService differentService) {
        this.differentService = differentService;
        customerAmount = differentService.getTotalRowNumber();
        dbNode = DbNode.DIFFERENT;
    }

    @Override
    public void run() {
        int i = 0;
        while (i < 5500) {
            try {
                addAdditionalTimeForInsertion();
                checkNewCustomerExistenceByAmount();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
            iteration++;
        }
    }

    private void addAdditionalTimeForInsertion() throws InterruptedException {
//        Thread.sleep(0);
//        System.out.println("LOG: additional time");
    }

    private void checkNewCustomerExistenceByAmount() {

        int newCustomerAmount;
        if (dbNode == DbNode.INSERTION)
            newCustomerAmount = selectionService.getTotalRowNumber();
        else
            newCustomerAmount = differentService.getTotalRowNumber();

        if (newCustomerAmount > customerAmount) {
            Timestamp timestamp = TimestampHelper.getCurrentTimestamp();
            customerAmount = newCustomerAmount;
//            System.out.println("LOG: " + (iteration - lastUpdate) + " addition logged :: Node__" + dbNode + "  " + timestamp + " ;;; previousTimestamp= " + previousTimestamp + ";;; customers=" + newCustomerAmount);
//            System.out.println(timestamp + ";" + (TimestampHelper.diff(previousTimestamp, timestamp)));
            lastUpdate = iteration;
        }
        previousTimestamp = TimestampHelper.getCurrentTimestamp();
    }

    private void waitUntilZeroSecond() throws InterruptedException {
        long timeToZero = 60 - TimestampHelper.getCurrentTimestamp().getSeconds();
        Thread.sleep(timeToZero * 1000);
        System.out.println("LOG: zero second");
    }


}
