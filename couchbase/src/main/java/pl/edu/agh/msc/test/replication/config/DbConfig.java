package pl.edu.agh.msc.test.replication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
public class DbConfig {

    @Bean(name = "dsNode1")
    @Primary
    @ConfigurationProperties(prefix = "database1.datasource")
    public DataSource dataSource1() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "dsNode2")
    @ConfigurationProperties(prefix = "database2.datasource")
    public DataSource dataSource2() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "tmNode1")
    @Autowired
    DataSourceTransactionManager transactionManager1(@Qualifier("dsNode1") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "tmNode2")
    @Autowired
    DataSourceTransactionManager transactionManager2(@Qualifier("dsNode2") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


}
