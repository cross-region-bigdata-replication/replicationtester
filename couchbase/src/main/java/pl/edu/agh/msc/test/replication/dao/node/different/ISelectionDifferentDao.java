package pl.edu.agh.msc.test.replication.dao.node.different;

import pl.edu.agh.msc.test.replication.helper.DbNode;
import pl.edu.agh.msc.test.replication.model.Customer;

import java.util.List;

public interface ISelectionDifferentDao {
    List<Customer> loadAllCustomer();

    Customer findCustomerById(long customerId);

    String findNameById(long customerId);

    int getCustomerAmount(DbNode dbNode);
}
