package pl.edu.agh.msc.test.replication.dao.node.insert;

import pl.edu.agh.msc.test.replication.model.IDbModel;

import java.util.List;

public interface IInsertionInsertDao {
    void initTable();

    void insert(IDbModel cus);

    void insertCustomerList(List<IDbModel> customerList);

}
