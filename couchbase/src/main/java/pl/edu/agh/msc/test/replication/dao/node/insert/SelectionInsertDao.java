package pl.edu.agh.msc.test.replication.dao.node.insert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msc.test.replication.helper.DbNode;
import pl.edu.agh.msc.test.replication.model.Customer;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SelectionInsertDao extends JdbcDaoSupport implements ISelectionInsertDao {

    @Qualifier("dsNode1")
    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public List<Customer> loadAllCustomer() {
        String sql = "SELECT * FROM customer";
        List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

        List<Customer> result = new ArrayList<>();
        for (Map<String, Object> row : rows) {
            Customer cus = new Customer();
            cus.setId((Long) row.get("cust_id"));
            cus.setName((String) row.get("name"));
//            cus.setAge((Integer) row.get("age"));
            cus.setT_s((Timestamp) row.get("t_s"));
            result.add(cus);
        }

        return result;
    }

    @Override
    public Customer findCustomerById(long customerId) {
        String sql = "SELECT * FROM customer WHERE CUST_ID = ?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{customerId}, (rs, rwNumber) -> {
            Customer cust = new Customer();
            cust.setId(rs.getLong("cust_id"));
            cust.setName(rs.getString("name"));
            cust.setAge(rs.getInt("age"));
            cust.setT_s(rs.getTimestamp("t_s"));
            return cust;
        });
    }

    @Override
    public String findNameById(long customerId) {
        String sql = "SELECT name FROM customer WHERE cust_id = ?";
        return getJdbcTemplate().queryForObject(sql, new Object[]{customerId}, String.class);
    }

    @Override
    public int getCustomerAmount(DbNode dbNode) {
        String sql = "SELECT Count(*) FROM customer";
        if (dbNode == DbNode.INSERTION) {
            return getCustomerAmountFromInsertionNode(sql);
        } else if (dbNode == DbNode.DIFFERENT) {
            //TODO: error handle
            System.out.println("ERROR: Bad node");
            return -1;
        }
        return 0;
    }


    @Transactional(value="tmNode1")
    private int getCustomerAmountFromInsertionNode(String sql) {
        return getJdbcTemplate().queryForObject(sql, Integer.class);
    }

    @Transactional(value="tmNode2")
    private int getCustomerAmountFromRandomNode(String sql) {
        return getJdbcTemplate().queryForObject(sql, Integer.class);
    }
}
