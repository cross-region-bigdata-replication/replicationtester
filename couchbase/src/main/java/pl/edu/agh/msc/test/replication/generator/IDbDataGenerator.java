package pl.edu.agh.msc.test.replication.generator;

import pl.edu.agh.msc.test.replication.model.Customer;
import pl.edu.agh.msc.test.replication.service.IInsertionService;

public interface IDbDataGenerator {

    boolean cleanTable(IInsertionService iService);

    Customer generateCustomer(IInsertionService iService);

}
