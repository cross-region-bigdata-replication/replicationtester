package pl.edu.agh.msc.test.replication.model;

import java.io.Serializable;
import java.sql.Timestamp;

public interface IDbModel extends Serializable {

    long getId();

    void setId(long id);

    Timestamp getT_s();

    void setT_s(Timestamp t_s);

}
