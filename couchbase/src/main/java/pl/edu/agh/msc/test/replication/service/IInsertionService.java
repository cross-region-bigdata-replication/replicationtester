package pl.edu.agh.msc.test.replication.service;

import pl.edu.agh.msc.test.replication.model.IDbModel;

import java.util.List;

public interface IInsertionService {

    void initTable();

    void insert(IDbModel modelObject);

    void insertRowList(List<IDbModel> modelObjectList);
}
