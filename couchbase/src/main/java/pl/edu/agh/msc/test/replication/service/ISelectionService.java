package pl.edu.agh.msc.test.replication.service;

import pl.edu.agh.msc.test.replication.model.Customer;

import java.util.List;

public interface ISelectionService {

    List<Customer> loadAll();

    Customer getCustomerById(long customerId);

    String getCustomerNameById(long customerId);

    int getTotalRowNumber();

}
