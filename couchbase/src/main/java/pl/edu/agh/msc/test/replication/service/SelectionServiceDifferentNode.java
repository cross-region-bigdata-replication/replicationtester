package pl.edu.agh.msc.test.replication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.msc.test.replication.dao.node.different.ISelectionDifferentDao;
import pl.edu.agh.msc.test.replication.helper.DbNode;
import pl.edu.agh.msc.test.replication.model.Customer;

import java.util.List;

@Service
public class SelectionServiceDifferentNode implements IDifferentService {

    @Autowired
    ISelectionDifferentDao customerDao;

    @Override
    public List<Customer> loadAll() {
        return null;
    }

    @Override
    public Customer getCustomerById(long customerId) {
        return null;
    }

    @Override
    public String getCustomerNameById(long customerId) {
        return null;
    }

    @Override
    public int getTotalRowNumber() {
        return customerDao.getCustomerAmount(DbNode.DIFFERENT);
    }
}
