package pl.edu.agh.msc.test.replication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.msc.test.replication.dao.node.insert.ISelectionInsertDao;
import pl.edu.agh.msc.test.replication.helper.DbNode;
import pl.edu.agh.msc.test.replication.model.Customer;
import pl.edu.agh.msc.test.replication.model.IDbModel;

import java.util.List;

@Service
public class SelectionServiceInsertionNode implements ISelectionService {

    @Autowired
    ISelectionInsertDao customerDao;

    @Override
    public List<Customer> loadAll() {
        List<Customer> listCust = customerDao.loadAllCustomer();
        for (IDbModel cus : listCust) {
            System.out.println(cus.toString());
        }
        return listCust;
    }

    @Override
    public Customer getCustomerById(long customerId) {
        return customerDao.findCustomerById(customerId);
    }

    @Override
    public String getCustomerNameById(long customerId) {
        return customerDao.findNameById(customerId);
    }

    @Override
    public int getTotalRowNumber() {
        return customerDao.getCustomerAmount(DbNode.INSERTION);
    }

}
