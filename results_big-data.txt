﻿CREATE KEYSPACE IF NOT EXISTS testdb WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '3' };
DROP TABLE IF EXISTS testdb.users ;
CREATE TABLE IF NOT EXISTS testdb.users ( lastname TEXT PRIMARY KEY, age SMALLINT, city TEXT, email TEXT, firstname TEXT );


===========================================================================
100% select
3 working nodes,
rf = 3

    private static final String OUTPUT_FILE_NAME = "generated_data.sql";
    private static final int LASTNAME_LENGTH = 500;
    private static final int FIRSTNAME_LENGTH = 300;
    private static final int CITY_LENGTH = 300;
    private static final int EMAIL_LENGTH = 500;
	
	so, one row size is: 1600 chars + age (0..99), 

0 (empty):

### Cassandra select big data count test
Sum: 449.000000; Mean: 4.401961; Median: 4.000000; Variance: 1.609105; Std: 1.268505
###
### Cassandra select big data count test ~Q
Sum: 492.000000; Mean: 4.823529; Median: 5.000000; Variance: 2.978451; Std: 1.725819
###
### Cockroach select big data count test
Sum: 352.000000; Mean: 3.450980; Median: 3.000000; Variance: 1.279752; Std: 1.131261
###


100 (120):



--------------------------------------------------------
1_000:

### Cassandra select big data count test
Sum: 697.000000; Mean: 6.833333; Median: 7.000000; Variance: 2.001650; Std: 1.414797
###
### Cassandra select big data count test ~Q
Sum: 871.000000; Mean: 8.539216; Median: 8.000000; Variance: 3.142011; Std: 1.772572
###
### Cockroach select big data count test
Sum: 618.000000; Mean: 6.058824; Median: 6.000000; Variance: 3.026209; Std: 1.739600
###



--------------------------------------------------------
10_000:

### Cassandra select big data count test
Sum: 3065.000000; Mean: 30.049020; Median: 28.500000; Variance: 70.819355; Std: 8.415424
###
### Cassandra select big data count test ~Q
Sum: 3681.000000; Mean: 36.088235; Median: 35.000000; Variance: 81.922831; Std: 9.051123
###
### Cockroach select big data count test
Sum: 2949.000000; Mean: 28.911765; Median: 28.000000; Variance: 39.625801; Std: 6.294903
###



--------------------------------------------------------
50_000:

### Cassandra select big data count test
Sum: 11931.000000; Mean: 116.970588; Median: 117.000000; Variance: 327.078334; Std: 18.085307
###
### Cassandra select big data count test ~Q
Sum: 13452.000000; Mean: 131.882353; Median: 131.000000; Variance: 424.976121; Std: 20.614949
###
### Cockroach select big data count test
Sum: 17595.000000; Mean: 172.500000; Median: 174.000000; Variance: 643.658416; Std: 25.370424
###

----------------------------------------------------------
100_000:

### Cassandra select big data count test
Sum: 60950.000000; Mean: 597.549020; Median: 592.500000; Variance: 12613.576781; Std: 112.310181
###
### Cassandra select big data count test ~Q
Sum: 66138.000000; Mean: 648.411765; Median: 655.000000; Variance: 9361.729761; Std: 96.756032
###
### Cockroach select big data count test
Sum: 36898.000000; Mean: 361.745098; Median: 365.500000; Variance: 3057.775966; Std: 55.297161
###

----------------------------------------------------------
250_000:

Cassandra nawet w trybie ONE niestety nie działa, nawet nie da się załadować tylu danych, a z załadowanymi 160k jest bardzo duży problem przy odczycie. 

### Cockroach select big data count test
Sum: 81935.000000; Mean: 803.284314; Median: 813.000000; Variance: 14284.759950; Std: 119.518869
### [ 8175196 total,  1627692 free,  2491956 used, ]

w Cassandra 100% RAM wykorzystane chyba [ 8175196 total,   124172 free,  7974404 used, ]
być może dlatego, coś przestało działać nagle? 
Cała wirtualka z Cassandra chodzi bardzo słabo. Cockroach nie ma tego problemu. Ewidentnie jest problem z RAM. 

Po DROP tabeli z danymi - zwolnienie dużej części pamięci RAM, ale nie do takiego poziomu jak cockroach, wciąż używane jest około 5GB. 


============================================================================================================

100% select
3 working nodes,
 rf = 1
 echo 'num_replicas: 1' | ./cockroach zone set .default --insecure -f -

    private static final String OUTPUT_FILE_NAME = "generated_data.sql";
    private static final int LASTNAME_LENGTH = 500;
    private static final int FIRSTNAME_LENGTH = 300;
    private static final int CITY_LENGTH = 300;
    private static final int EMAIL_LENGTH = 500;
	
	so, one row size is: 1600 chars + age (0..99), 

0: (after 250k)

### Cassandra select big data count test (po 250k, new też jednak)
Sum: 1968.000000; Mean: 19.294118; Median: 16.000000; Variance: 95.259173; Std: 9.760081
###
### Cockroach select big data count test (po 250k)
Sum: 351.000000; Mean: 3.441176; Median: 3.000000; Variance: 1.773733; Std: 1.331816
###

100 (120): (after 250k)


### Cassandra select big data count test
Sum: 1714.000000; Mean: 16.803922; Median: 15.000000; Variance: 68.139390; Std: 8.254659
###
### Cockroach select big data count test
Sum: 408.000000; Mean: 4.000000; Median: 4.000000; Variance: 3.089109; Std: 1.757586
###


1000: (test executed after 250k)


### Cassandra select big data count test
Sum: 1900.000000; Mean: 18.627451; Median: 17.000000; Variance: 52.156863; Std: 7.221971
###
### Cockroach select big data count test
Sum: 640.000000; Mean: 6.274510; Median: 6.000000; Variance: 3.072413; Std: 1.752830
###



10_000:
### Cassandra select big data count test
Sum: 3332.000000; Mean: 32.666667; Median: 31.000000; Variance: 138.066007; Std: 11.750149
###
### Cockroach select big data count test
Sum: 3086.000000; Mean: 30.254902; Median: 30.000000; Variance: 27.340322; Std: 5.228797
###

100_000:
### Cassandra select big data count test
Sum: 8858.000000; Mean: 86.843137; Median: 84.000000; Variance: 289.183071; Std: 17.005384
###
### Cockroach select big data count test
Sum: 35471.000000; Mean: 347.754902; Median: 353.000000; Variance: 2860.602699; Std: 53.484602
###

Cassandra przy 100k rows, ma znów duże zużycie RAM, ale nie krytyczne bo :  8175196 total,   348028 free,  7272096 used,

Cockroach:  8175196 total,  3979624 free,  1677652 used, 

170_000: 


### Cassandra select big data count test
Sum: 14221.000000; Mean: 139.421569; Median: 139.500000; Variance: 556.483887; Std: 23.589911
###
### Cockroach select big data count test
Sum: 63054.000000; Mean: 618.176471; Median: 631.000000; Variance: 9362.483401; Std: 96.759927
###

250_000:
Cassandra przy 235k ma problem z timeout, nie daje się załadować więcej danych bez zmiany domyślnych ustawień,
prawdopodobnie znów nastąpił nagły spadek wydajności ze względu na zużycie RAM.

przy 235k w Cassandra i 240k w Cockroach: 
do Cockroach można dalej załadować dane, ile sie chce:

### Cassandra select big data count test
Sum: 59584.000000; Mean: 584.156863; Median: 546.000000; Variance: 115726.232576; Std: 340.185585
###
### Cockroach select big data count test
Sum: 85110.000000; Mean: 834.411765; Median: 842.500000; Variance: 16199.214910; Std: 127.276136
###

ubuntu@cassandrainstance:~$ sudo docker exec -it cassandra-seed-node nodetool gettimeout read
Current timeout for type read: 5000 ms
ubuntu@cassandrainstance:~$ sudo docker exec -it cassandra-seed-node nodetool gettimeout write
Current timeout for type write: 2000 ms

następnie wykonano: 
ubuntu@cassandrainstance:~$ sudo docker exec -it cassandra-seed-node nodetool settimeout write 7000
ubuntu@cassandrainstance:~$ sudo docker exec -it cassandra-seed-node nodetool settimeout read 5000
 aby doładować danych do 250k. 
 
po zwiększeniu timeoutu, uzyskano następujące wyniki: 

### Cassandra select big data count test
Sum: 55170.000000; Mean: 540.882353; Median: 547.500000; Variance: 6656.540478; Std: 81.587625
###
### Cockroach select big data count test
Sum: 88931.000000; Mean: 871.872549; Median: 884.500000; Variance: 17449.538051; Std: 132.096700
###


Warto jednocześnie zaznaczyć, że po załadowaniu dużych danych (aż do osiągnięcia 
stanu zauważalnego spadku wydajności) do Cassandra a następnie ich usunięciu, ciężkie lub niemożliwe może okazać się,
osiągnięcie stanu pierwotnej wydajności.   (patrz dla 0) <- chyba jedak nieprawda, dalej jest tak cienko przy rf=1, jak się zmieni na rf=3 nawet w trakcie pracy, to wyniki są podobne do tych z poprzedniej sekcji gdzie testowałem cassandre dla rf=3.

ALTER KEYSPACE "testdb" WITH REPLICATION =
  { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };
