# Update apt-get
sudo apt-get --yes update

# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get --yes install \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common

# Add Docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Verify
sudo apt-key fingerprint 0EBFCD88

# Set up the stable repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update the apt pacakge index
sudo apt-get --yes update

# Install latest version of Docker CE
sudo apt-get --yes install docker-ce

# Install latest version of Docker Compose
sudo apt-get --yes install docker-compose

# Create dir

mkdir mgr
cd mgr
mkdir cassandra
cd cassandra

# Get cassandra's docker-compose file 
sudo wget https://gdurl.com/9Ztc

# Run docker-compose
sudo docker-compose up