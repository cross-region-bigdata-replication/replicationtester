#!/usr/bin/expect -f

set timeout -1
log_file output260803.txt

### S Check
spawn java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
puts "****** SIZE: 0 ABOVE ******"
#expect eof
### E Check


for {set x 1} {$x < 21} {incr x} {
### S Add
spawn timeout 7200 java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "0\r"
expect -exact "0\r
---Database generator, refiller---\r
Put the amount of rows to fill\r
"
send -- "10000\r"
expect -exact "10000\r"
expect eof
### E Add

for {set y 0} {$y < 5} {incr y} {
### S Check
spawn timeout 7200 java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
puts "****** ITER: $x:$y ABOVE ******"
#expect eof
### E Check
}
}
