#!/usr/bin/expect -f

set timeout -1
log_file output310801.txt


for {set y 0} {$y < 5} {incr y} {
### S Check
spawn timeout 7200 java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
puts "****** ITER: $y ABOVE ******"
#expect eof
### E Check
}

