#!/usr/bin/expect -f

set timeout -1
log_file output040801.txt

### S Check
spawn java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
### E Check

#-------------------------------------------------------------
### S Add
spawn timeout 600 java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "0\r"
expect -exact "0\r
---Database generator, refiller---\r
Put the amount of rows to fill\r
"
send -- "10000\r"
expect -exact "10000\r"
expect eof
### E Add

### S Check
spawn java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
### E Check

#-------------------------------------------------------------
### S Add
spawn timeout 600 java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "0\r"
expect -exact "0\r
---Database generator, refiller---\r
Put the amount of rows to fill\r
"
send -- "50000\r"
expect -exact "50000\r"
expect eof
### E Add

### S Check
spawn java -jar repl_tester.jar
match_max 100000
expect -exact "Choose the running mode (0 - database generator, refiller; 1 - database tester\r
"
send -- "1\r"
expect -exact "1\r"
expect eof
### E Check
