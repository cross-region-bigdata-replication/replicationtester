---
# INFO: The first version of the Ansible config file. Still start all the services on one machine.
# Playbook with ansible initialization
- name: Initialize the python 2.7 on ubuntu 16.04 for ansible 
  hosts: all
  become: true
  gather_facts: False

  tasks:
  - name: install python 2
    raw: test -e /usr/bin/python || (apt -y update && apt install -y python-minimal)

# Playbook with docker and docker-compose installation 
- name: Install docker and docker-compose on the machine 
  hosts: all
  become: true

  tasks:
  - name: install python 2
    raw: test -e /usr/bin/python || (apt -y update && apt install -y python-minimal)
  - name: Update apt-get
    command: sudo apt-get --yes update
    ignore_errors: True
  - name: Install packages to allow apt to use a repository over HTTPS
    command: sudo apt-get --yes install apt-transport-https ca-certificates curl software-properties-common
    ignore_errors: True
  - name: Add Docker's official GPG key
    command: curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    ignore_errors: True
  - name: Verify the GPG key from Docker
    command: sudo apt-key fingerprint 0EBFCD88
  - name: Set-up the stable repository 
    command: sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    ignore_errors: True
  - name: Update the apt package index 
    command: sudo apt-get --yes update
    ignore_errors: True
  - name: Install latest version of Docker CE
    command: sudo apt-get --yes install docker-ce
    ignore_errors: True
  - name: Install latest version of Docker Compose
    command: sudo apt-get --yes install docker-compose
    ignore_errors: True
# Update docker engine if not the newest
- name: Update the docker engine to the latest version
  hosts: all
  become: true
  tasks:
  - name: get gpg
    shell: > 
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  - name: set apt-repository with docker for ubuntu
    shell: > 
      sudo add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable"
  - name: apt-get update
    shell: > 
      sudo apt-get update
  - name: install docker-ce
    shell: > 
      sudo apt-get install docker-ce

# Playbook with directories creation (ON CLUSTER)
- name: Configure machine filesystem and prepare for nodes run
  hosts: cluster
  become: true
  vars:
    solution_system: cockroach
    solution_type: cluster

  tasks:  
  - name: Create directory tree
    file: path="mgr/{{ solution_system }}/" state=directory
  - name: Copy the docker-compose file to the remote location
    copy:
      src: "mgr/{{ solution_system }}/docker-compose.yml.{{solution_type}}"
      dest: "mgr/{{ solution_system }}/docker-compose.yml"
  - name: Copy loadbalancer HAProxy file to the remote location
    copy:
      src: ./haproxy.cfg
      dest: "mgr/{{ solution_system }}/haproxy.cfg"

# Playbook with directories creation (ON NODES)
- name: Configure machine filesystem and prepare for nodes run
  hosts: nodes
  become: true
  vars:
    solution_system: cockroach
    solution_type: node
    cluster_address: 149.156.11.4
    cluster_port: 10408

  tasks:  
  - name: Create directory tree
    file: path="mgr/{{ solution_system }}/" state=directory
  - name: Copy the docker-compose file to the remote location
    copy:
      src: "mgr/{{ solution_system }}/docker-compose.yml.{{solution_type}}"
      dest: "mgr/{{ solution_system }}/docker-compose.yml"
  - name: Replace CLUSTER_ADDRESS with real cluster address
    replace: 
      path: "mgr/{{ solution_system }}/docker-compose.yml"
      regexp: "CLUSTER_ADDRESS"
      replace: "{{cluster_address}}:{{cluster_port}}"

# Playbook with docker creation and run
- name: Run the Cockroach docker containers on machines 
  hosts: all
  become: true
  vars:
    solution_system: cockroach

  tasks:
  - name: Find out playbook's path
    shell: pwd
    register: playbook_path_output
  - debug: 
    debug: var=playbook_path_output.stdout
  - name: Down docker-compose services (docker-compose down)
    shell: sudo docker-compose down -d
    args:
      chdir: "mgr/{{ solution_system }}/"
  - name: Run docker-compose services (docker-compose up)
    shell: sudo docker-compose up -d
    args:
      chdir: "mgr/{{ solution_system }}/"
